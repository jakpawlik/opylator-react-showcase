// ##############################
// // // Dashboard styles
// #############################

import { successColor } from ".";

const dashboardStyle = {
  successText: {
    color: successColor
  },
  upArrowCardCategory: {
    width: 14,
    height: 14
  }
};

export default dashboardStyle;
