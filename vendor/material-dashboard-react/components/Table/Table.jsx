import React from "react";
import {
  withStyles,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell
} from "material-ui";

import PropTypes from "prop-types";

import tableStyle from "../../variables/styles/tableStyle";

function printRow(tableHead, row, classes, th = false){
  let key = 0;
  let result = [];
  for(let prop in tableHead) {
    result.push(<TableCell
        className={classes.tableCell + " " + (th ? classes.tableHeadCell : '')}
        key={'row_item_' + key}
      >
        {row[prop]}
      </TableCell>);
    key++;
  }

  return result;
}

function printRows(tableHead, rows, classes){
  let result = [];

  for(let key in rows) {
    result.push(<TableRow className={classes.tableRow} key={'table_row_' + key}>
      {printRow(tableHead, rows[key], classes)}
    </TableRow>);
  }

  return result;
}

function CustomTable({ ...props }) {
  const { classes, tableHead, tableData, tableHeaderColor } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow className={classes.tableHeadRow}>{printRow(tableHead, tableHead, classes, true)}</TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {printRows(tableHead, tableData, classes)}
        </TableBody>
      </Table>
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ])
};

export default withStyles(tableStyle)(CustomTable);
