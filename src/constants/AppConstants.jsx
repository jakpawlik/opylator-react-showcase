const BASE_URL = document.getElementsByTagName('base')[0].getAttribute('href');

export default {
  LOADING: 'LOADING',
  routes: {
    __ROOT_PATH: '/admin',
    __LOGIN_PATH: '/admin/login',
    __LOGOUT_PATH: '/admin/logout',
    __RESEND_PATH: '/admin/resend',
    ticket: {
      __INDEX_PATH: '/admin/ticket',
    },
    event: {
      __INDEX_PATH: '/admin/event',
      __CREATE_PATH: '/admin/event/create',
      __EDIT_PATH: '/admin/event/edit',
      __PUBLIC: '/event/'
    }
  },
  BASE_URL: BASE_URL,
  LOGIN_URL: BASE_URL + 'sessions/create',
  SIGNUP_URL: BASE_URL + 'users',
  LOGIN_USER: 'LOGIN_USER',
  LOGOUT_USER: 'LOGOUT_USER',
  SCANNER_ENABLE: 'SCANNER_ENABLE',
  SCANNER_DISABLE: 'SCANNER_DISABLE',
  REDIRECT: 'REDIRECT'
};
