import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import UserApp from './components/UserApp';
import LoginActions from './modules/Login/actions/LoginActions';
import { Router } from 'react-router-dom';
import History from './services/History';
import LoginStore from './modules/Login/stores/LoginStore';
import IntlTools from './services/IntlTools';

let jwt = LoginStore.jwt;

if (jwt) {
  LoginActions.loginUser(jwt);
}


if(document.getElementById('admin-app')) {
  let indexContent =
    <Router history={History.history}>
      <App/>
    </Router>;

  ReactDOM.render((
    IntlTools.renderIntlProvider(indexContent)
  ), document.getElementById('admin-app'));

  require('../../assets/less/app.less');
} else {
  let indexContent =
    <Router history={History.history}>
      <UserApp/>
    </Router>;

  ReactDOM.render((
    IntlTools.renderIntlProvider(indexContent)
  ), document.getElementById('app'));

  require('../../assets/less/front.less');
}

