import React from 'react';
import LoginAPI from '../../services/LoginAPI';
import { Send } from 'material-ui-icons';
import { Input, Card, CardHeader } from 'material-ui';
import Auth from '../../services/Auth';
import { Redirect } from 'react-router-dom';
import { withStyles } from 'material-ui';
import Button from 'material-ui/Button';
import { FormattedMessage } from 'react-intl';
import IntlTools from '../../../../services/IntlTools';
import AppConstants from '../../../../constants/AppConstants';

import LoginStyle from './LoginStyle';
import LoginStore from "../../stores/LoginStore";

const initialState = {
    error: null,
    authenticated: false,
    login: {
        username: '',
        password: ''
    }
};

class LoginFormComponent extends React.Component {

  constructor() {
    super();
    this.state = initialState;
  }

  componentWillMount(){
    if(Auth.isLoggedIn()){
      this.setState({authenticated: true});
    }
  }

  componentDidMount(){
    let _self = this;

    LoginStore.addChangeListener((e) => {
      if(e.logged_in){
        _self.setState({authenticated: true});
        document.location.reload();
      }
    });
  }

  login(e){
    e.preventDefault();
    let loginInfo = LoginAPI.login(this.state.login.username, this.state.login.password);

    loginInfo.catch(function() {
      alert('There\'s an error logging in');
    });
  }

  handleChange(e){
    let val = this.state.login;
    val[e.target.name] = e.target.value;
    this.setState({'login': val });
  }

  render() {
    const { classes } = this.props;
    if(this.state.authenticated){
      return <Redirect to='/admin/' />;
    }

    return (
      <div className='container small'>
        <Card>
          <CardHeader
            title={IntlTools.trans('app.admin_panel')}
            className='text-center'
          />
        <form className='loginForm' method="POST" onSubmit={this.login.bind(this)}>
          <Input
              label='Username'
              id='username'
              value={this.state.login.username}
              name='username'
              placeholder={IntlTools.trans('app.login.username')}
              className='flex-block form-control-elem'
              onChange={this.handleChange.bind(this)}
          />
          <Input
              label='Password'
              id='password'
              name='password'
              placeholder={IntlTools.trans('app.login.password')}
              type='password'
              value={this.state.login.password}
              className='flex-block form-control-elem'
              onChange={this.handleChange.bind(this)}
          />
          <div className='text-center'>
            <Button type="submit" className={classes.button} variant="raised" color="primary">
              <FormattedMessage
                id="app.login.login"
                defaultMessage={'Log in'} />

              <Send className={classes.icon} />
            </Button>
          </div>
          </form>
        </Card>
      </div>
    );
  }
}

export default withStyles(LoginStyle)(LoginFormComponent);
