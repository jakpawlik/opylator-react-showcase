import AppDispatcher from '../../../dispatchers/AppDispatcher';
import AppConstants from '../../../constants/AppConstants';
import RedirectActions from '../../../actions/RedirectActions';

export default {
  loginUser: (jwt) => {
    RedirectActions.redirect(AppConstants.routes.__ROOT_PATH);
    AppDispatcher.dispatch({
      actionType: AppConstants.LOGIN_USER,
      jwt: jwt
    });
  },
  logoutUser: () => {
    AppDispatcher.dispatch({
      actionType: AppConstants.LOGOUT_USER
    });
    RedirectActions.redirect(AppConstants.routes.__LOGIN_PATH);
  }
}
