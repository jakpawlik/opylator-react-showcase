import LoginActions from '../actions/LoginActions';
import axios from 'axios';
const LOGIN_URL = '/api/login/check';

class LoginAPI {
  login(username, password) {

    let promise = axios.post(LOGIN_URL, {
        username, password
    });

    return this.handleAuth(promise);
  }

  logout() {
    LoginActions.logoutUser();
  }

  signup() {
  }

  handleAuth(loginPromise) {
    return loginPromise
      .then(function(response) {
        var jwt = response.data.token;
        LoginActions.loginUser(jwt);
        return true;
      });
  }
}

export default new LoginAPI();
