import LoginStore from '../stores/LoginStore';
import RedirectActions from '../../../actions/RedirectActions';

class Auth {

    getToken(){
      return LoginStore.jwt;
    }

    constructor(){
      this.jwt = this.getToken();
    }

    isLoggedIn(){
      return !!this.getToken();
    }

    redirectIfLoggedIn(path){
      if(this.isLoggedIn()){
          this.redirect(path);
      }
    }

    redirectIfNotLoggedIn(path){
      if(!this.isLoggedIn()){
          this.redirect(path);
      }
    }
    redirect(path){
      RedirectActions.redirect(path);
    }
}

export default new Auth();
