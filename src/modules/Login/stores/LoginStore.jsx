import Store from '../../../stores/Store';
import StorageManager from '../../../services/StorageManager';
import AppConstants from '../../../constants/AppConstants';
import jwt_decode from 'jwt-decode';

let _user = null;
let _jwt = null;

const USE_STORAGE = true;

class LoginStoreClass extends Store {

  constructor() {
    super();

    if(USE_STORAGE){
      let storedJwt = StorageManager.getItem('jwt');

      if(storedJwt && _jwt === null){
        this.setUser(storedJwt);
      }
    }
  }

  clear(){
    _user = null;
    _jwt = null;

    if(USE_STORAGE){
        StorageManager.removeItem('jwt');
    }
  }

  setUser(jwt){
    _user = jwt_decode(jwt);
    _jwt = jwt;

    if(USE_STORAGE){
      let storedJwt = StorageManager.getItem('jwt')

      if(!storedJwt || storedJwt !== jwt){
        StorageManager.setItem('jwt', jwt);
      }
    }
  }

  get user() {
    return _user;
  }

  get jwt() {
    return _jwt;
  }

  isLoggedIn() {
    return !!this._user;
  }
}

const LoginStore = new LoginStoreClass();

LoginStore.subscribe((payload) => {
  switch (payload.actionType) {
    case AppConstants.LOGIN_USER:
        LoginStore.setUser(payload.jwt);
        LoginStore.emitChange({
          logged_in: true
        });
    break;

    case AppConstants.LOGOUT_USER:
        LoginStore.clear();
        LoginStore.emitChange({
          logged_out: true
        });
    break;
  }
});

Object.freeze(LoginStore);
export default LoginStore;
