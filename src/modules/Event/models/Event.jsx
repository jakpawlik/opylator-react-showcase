import {fk, many, attr, Model} from 'redux-orm';

class Event extends Model {

}

Event.modelName = 'Event';

// Declare your related fields.
Event.fields = {
    id: attr(), // non-relational field for any value; optional but highly recommended
    name: attr(),
};

export default Event;
