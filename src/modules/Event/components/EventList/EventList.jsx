import React from 'react';
import AppConstants from '../../../../constants/AppConstants';

import {defineMessages} from 'react-intl';
import IntlComponent from '../../../../components/Intl/Intl';

import Table from '../../../../components/Table/Table';
import Link from '../../../../components/Link/Link';
import { withStyles } from 'material-ui';
import Button from 'muicss/lib/react/button';
import EventListStyle from './EventListStyle';
import EventStore from '../../stores/EventStore';
import EventApi from '../../services/EventApi';
import FormComponent from '../../../../components/Form/Form';
import Moment from "moment";
import Modal from 'react-modal';
import IntlTools from '../../../../services/IntlTools';
import TicketStore from "../../../Ticket/stores/TicketStore";
import {Redirect} from "react-router-dom";
import TicketApi from "../../../Ticket/services/TicketApi";
import EventActions from "../../actions/EventActions";
import EventConstants from "../../constants/EventConstants";

const initialState = {
    items: [],
    modalIsOpen: false,
    eventTickets: null,
    form: {
      n: '',
      price: '',
      pool: 1,
      id: null
    }
};

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

Modal.setAppElement(document.getElementById('admin-app')?'#admin-app':'#app');

class EventListComponent extends FormComponent {

  constructor(props){
    super(props);
    this.state = initialState;
    this.formData = {
      name: 'generateTickets',
      classes: [],
      submitLabel: 'app.event.generate_tickets',
      fieldClasses: ['mdc-form-field', 'mdc-form-field--align-end'],
      fields: {
        n: {
          type: 'text',
          classes: [],
          label: 'app.event.ticketCount'
        },
        price: {
          type: 'text',
          classes: [],
          label: 'app.event.ticketPrice'
        },
        pool: {
          type: 'number',
          classes: [],
          label: 'app.event.pool'
        },
        id: {
          type: 'hidden',
          classes: [],
          label: false
        }
      }
    };
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.list_updated){
        _self.setState({items: EventStore.getItems()});
      }

      if(e.tickets_generated){
        _self.setState({eventTickets: e.event.data.event});
      }

      if(e.actionType === EventConstants.EVENT_LIST_MODAL){
        _self.setState({modalIsOpen: e.modal});
        if(e.modal) {
          _self.setState({form: {id: e.eventId}});
        }
      }
    });

    EventApi.getIndex();
  }

  componentWillMount(){
    this.__header = {
      id: 'Id',
      name: this.__f('app.event.name'),
      place: this.__f('app.event.place'),
      date: this.__f('app.event.date'),
      tickets_count: this.__f('app.event.ticketCount'),
      tickets_available_count: this.__f('app.event.availableTicketCount')
  };

    this.transformers = EventStore.getTransformers();
  }

  __f(txt){
    return IntlTools.trans(txt);
  }

  openModal(element){
    EventActions.openEventTicketsModal(element);
  }

  delete(element){
    if(confirm('Czy na pewno chcesz usunąć wydarzenie?')){
      EventApi.delete(element);
    }
  }

  afterOpenModal(){
  }

  closeModal(){
    EventActions.closeEventTicketsModal();
  }

  toggleEvent(element) {
    EventApi.toggle(element.id);
  }

  itemActions(element) {
    let showRoute = AppConstants.routes.event.__INDEX_PATH + '/' + element.id;
    let editRoute = AppConstants.routes.event.__EDIT_PATH + '/' + element.id;

    let ticketsRoute = AppConstants.routes.event.__INDEX_PATH + '/tickets/' + element.id;

    return <div>
      <Link to={showRoute} text={'app.event.show'} />
      <Link to={editRoute} text={'app.event.edit'} />
      <Button color='danger' onClick={() => this.delete(element)}>{this.__f('app.event.delete')}</Button>
      <Link to={ticketsRoute} text={'app.event.tickets'} />

      <Button color='primary' onClick={() => this.toggleEvent(element)}>{this.__f( element.disabled ? 'app.event.enable' : 'app.event.disable')}</Button>

      <Button color='primary' onClick={() => this.openModal(element)}>{this.__f('app.event.generate_tickets')}</Button>
    </div>;
  }

  handleSubmit(e){
    e.preventDefault();

    this.create(e);
  }

  create(e){
    e.preventDefault();
    let eventInfo = EventApi.generate(this.state.form.n, this.state.form.price, this.state.form.pool, this.state.form.id);

    eventInfo.catch(function(e) {
      console.log(e);
    });
  }

  render() {
    let _self = this;
    if(!!this.state.eventTickets){
      let ticketsRoute = AppConstants.routes.event.__INDEX_PATH + '/tickets/' + _self.state.eventTickets;
      return <Redirect to={ticketsRoute}/>
    }

    return (
      <div>
        <Link to={AppConstants.routes.event.__CREATE_PATH} text={'app.event.create'}/>
        <Table elements={EventStore.addTransformers(this.state.items)} excluded={['ticketPrice']} headers={this.__header} actions={this.itemActions.bind(this)} />
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel={this.__f('app.event.generate_tickets')}
          style={customStyles}
        >
          {this.buildForm()}
        </Modal>
      </div>
    );
  }
}

export default withStyles(EventListStyle)(EventListComponent);
