const EventListStyle = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  icon: {
    marginLeft: theme.spacing.unit
  }
});

export default EventListStyle;
