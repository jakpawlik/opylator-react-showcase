import React from 'react';

import {defineMessages} from 'react-intl';
import IntlComponent from '../../../../components/Intl/Intl';

import { withStyles } from 'material-ui';
import Button from 'muicss/lib/react/button';
import EventShowStyle from './EventShowStyle';
import EventStore from '../../stores/EventStore';
import EventApi from '../../services/EventApi';

import moment from 'moment';
import TicketStore from "../../../Ticket/stores/TicketStore";
import TicketApi from "../../../Ticket/services/TicketApi";

import {Line, Bar} from 'react-chartjs-2';

import randomColor from './randomColor';

const initialState = {
    data: null
};

const loader = require('../../../../../../assets/svg/loading.svg');

let messages = defineMessages({
  eventShow: {
    id: 'app.event.show',
    defaultMessage: 'EventShow data',
    tickets: [],
    poolData: {},
    loading: true
  }
});

class EventShowComponent extends IntlComponent {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentWillMount(){
    this.addMessages(messages);
  }

  setStatData(){
    let tickets = this.state.tickets;
    let pools = {};

    for(let key in tickets){
      let ticket = tickets[key];

      let poolKey = 'pool' + ticket.pool;

      let datefield = moment(ticket.sold_at ? ticket.sold_at : ticket.createdAt).format('Y-MM-D');

      if(!!pools[poolKey]){
        if(pools[poolKey][datefield]){
          pools[poolKey][datefield]['total'] += 1;

          if(ticket.sold == 'TAK'){
            pools[poolKey][datefield]['sold'] += 1;
          }

        }else{
          pools[poolKey][datefield] =  {
            total: 1,
            sold: ticket.sold == 'TAK' ? 1 : 0
          };
        }
      }else{
        pools[poolKey] = {};
        pools[poolKey][datefield] = {
          total: 1,
          sold: ticket.sold == 'TAK' ? 1 : 0
        };
      }
    }

    this.setState({poolData: pools});
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.data_updated){
        _self.setState({data: EventStore.getCurrentItem()});
        TicketApi.getEventTicketsListAll(this.props.match.params.id);
      }
    });

    TicketStore.addChangeListener((e) => {
      if(e.list_clear){
        _self.setState({tickets: []});
      }

      if(e.list_updated){
        _self.setState({tickets: TicketStore.getItems(), loading: false});
        this.setStatData();
      }
    });
    
    EventApi.show(this.props.match.params.id);
  }

  sendToReport(){
    let url = '/admin/event/report/' + this.props.match.params.id;

    let win = window.open(url, '_blank');
    win.focus();
  }

  printData(){
    return <div id={'eventData'}>
      <h1>{this.state.data.name}</h1>
      <h2>{this.state.data.place}</h2>
      <h3>{moment(this.state.data.date).format('Y-MM-D H:mm')}</h3>
      <p>{this.state.data.description}</p>
      <Button color={'primary'} onClick={this.sendToReport.bind(this)}>Generuj raport (.XSLX)</Button>
    </div>;
  }

  printPoolData(){
    let pools = [];

    for(let key in this.state.poolData){
      let poolkey = key.replace('pool', '');
      let poolData = {
        total: 0,
        sold: 0
      };

      for(let key2 in this.state.poolData[key]){
        poolData.total += this.state.poolData[key][key2].total;
        poolData.sold += this.state.poolData[key][key2].sold;
      }

      pools.push(<tr key={poolkey}>
        <td style={{textAlign: 'center'}}>{poolkey}</td>
        <td style={{textAlign: 'center'}}>{poolData.total}</td>
        <td style={{textAlign: 'center'}}>{poolData.sold}</td>
      </tr>);
    }

    return pools.map((elem, i) => {
      return elem;
    })
  }

  getKey(value, data){
    let result = -1;

    for(let key in data){
      if(data[key] === value){
        return parseInt(key);
      }
    }

    return result;
  }

  printStatistics(){
    let allQty = 0;
    let soldQty = 0;

    for(let key in this.state.poolData){
      let pool = this.state.poolData[key];

      for(let key2 in pool){
        let info = pool[key2];

        allQty += info.total;
        soldQty += info.sold;
      }
    }

    let chartData = { labels: [], datasets: []};

    for(let poolKey in this.state.poolData){
      for(let dataKey in this.state.poolData[poolKey]) {

        let info = this.state.poolData[poolKey][dataKey];

        let inLabels = dataKey in chartData.labels;
        let add = true;

        for(let labelKey in chartData.labels){
          if(dataKey == chartData.labels[labelKey]){
            add = false;
          }
        }

        if(!info.sold){
          add = false;
        }

        if(add) {
          chartData.labels.push(dataKey);
        }
      }
    }

    chartData.labels.sort();

    let getRandomColor = function() {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    };

    let i = 0;
    for(let poolKey in this.state.poolData){
      let autoData = [];

      for(let i=0; i < chartData.labels.length; i++){
        autoData.push(0);
      }

      let chartSourceSold =  {
        label: "Sprzedane z puli " + poolKey.replace('pool', ''),
        fillColor: getRandomColor(),
        backgroundColor: getRandomColor(),
        strokeColor: '#000000',
        data: autoData
      };

      for(let dataKey in this.state.poolData[poolKey]){
          let item = this.state.poolData[poolKey][dataKey];

          let key = this.getKey(dataKey, chartData.labels);

          if(item.sold){
            chartSourceSold.data[key] = item.sold;
          }
      }

      chartData.datasets.push(chartSourceSold);

      i += 2;
    }

    let chartOptions = {
      scaleShowGridLines: true,
      maintainAspectRatio: true,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    };

    return <div id={'eventStatistics'}>
      <table style={{
        width: '100%'
      }}>
        <tbody>
          <tr>
            <th colSpan={2} style={{fontWeight: 'bold', backgroundColor: '#CCC'}}>Ilość biletów</th>
            <td style={{textAlign: 'center'}}>{allQty}</td>
          </tr>
          <tr>
            <th style={{fontWeight: 'bold', backgroundColor: '#CCC'}} colSpan={2}>Ilość sprzedanych biletów</th>
            <td  style={{textAlign: 'center'}}>{soldQty}</td>
          </tr>
          <tr>
            <th style={{fontWeight: 'bold', backgroundColor: '#CCC'}}>Pula</th>
            <th style={{fontWeight: 'bold', backgroundColor: '#CCC'}}>Ilość biletów</th>
            <th style={{fontWeight: 'bold', backgroundColor: '#CCC'}}>Ilość sprzedanych biletów</th>
          </tr>
          {this.printPoolData()}
        </tbody>
      </table>
      <Bar
        data={chartData}
        width={500}
        height={300}
        options={chartOptions}
      />
    </div>;
  }

  render(){
    return <div>
      {this.state.data ? this.printData() : null}
      {this.state.loading ? <div style={{textAlign: 'center'}}><img src={loader} /><br/><strong>{this.__f('app.loading')}...</strong></div> : ''}
      {this.state.poolData ? this.printStatistics() : null}
    </div>;
  }
}

export default withStyles(EventShowStyle)(EventShowComponent.intl());
