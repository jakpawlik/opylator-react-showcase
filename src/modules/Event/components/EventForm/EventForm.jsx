import React, { Fragment } from 'react';
import AppConstants from '../../../../constants/AppConstants';

import {defineMessages} from 'react-intl';
import FormComponent from '../../../../components/Form/Form';

import Link from '../../../../components/Link/Link';
import { withStyles, Button } from 'material-ui';

import EventFormStyle from './EventFormStyle';
import EventStore from '../../stores/EventStore';
import EventApi from '../../services/EventApi';
import Auth from "../../../Login/services/Auth";
import {Redirect} from "react-router-dom";
import Moment from 'moment';
import Form from "muicss/lib/react/form";
import RedirectActions from "../../../../actions/RedirectActions";
import TicketApi from "../../../Ticket/services/TicketApi";
import Dropzone from "react-dropzone";
import classNames from "classnames";

const initialState = {
  created: false,
  data: null,
  loaded: 0,
  files: [],
  form: {
    name: '',
    date: '',
    place: '',
    description: '',
    payment_fee: 0.26,
    service_fee: 2.20,
    repeated_fee: 0
  }
};

class EventFormComponent extends FormComponent {
  constructor(props){
    super(props);
    this.state = initialState;
    this.formData = {
      name: 'event',
      classes: [],
      submitLabel: 'app.event.submit',
      fieldClasses: ['mdc-form-field', 'mdc-form-field--align-end'],
      fields: {
        name: {
          type: 'text',
          classes: [],
          label: 'app.event.name'
        },
        place: {
          type: 'text',
          classes: [],
          label: 'app.event.place'
        },
        date: {
          type: 'datetime',
          classes: [],
          label: 'app.event.date_form',
          placeholder: 'Y-m-d H:m',
          // transform: function(value){
          //   Moment.locale('pl');
          //   return Moment(value).toDate();
          // }
        },
        description: {
          type: 'textarea',
          classes: [],
          label: 'app.event.description'
        },
        payment_fee: {
          type: 'text',
          classes: [],
          label: 'app.event.paymentFee',
          transform: function(value){
            if(typeof value === 'string') {
              return value.replace(',', '.');
            }

            return value;
          }
        },
        service_fee: {
          type: 'text',
          classes: [],
          label: 'app.event.serviceFee',
          transform: function(value){
            if(typeof value === 'string') {
              return value.replace(',', '.');
            }

            return value;
          }
        },
        repeated_fee: {
          type: 'text',
          classes: [],
          label: 'app.event.repeatedFee',
          transform: function(value){
            if(typeof value === 'string') {
              return value.replace(',', '.');
            }

            return value;
          }
        },
        file: {
          type: 'file',
          classes: [],
          label: 'app.event.file',
          placeholder: 'Y-m-d H:m'
        },
      }
    };
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.data_created){
        _self.setState({
          created: true,
          data: null,
          loaded: 0,
          files: [],
          form: {
            name: '',
            date: '',
            place: '',
            description: ''
          }
        });
      }
    });

    EventStore.addChangeListener((e) => {
      if(e.data_updated){
        let data = EventStore.getCurrentItem();
        let form = {};
        form = data;
        let image = !!data.image ? data.image : null;
        delete form.image;

        _self.setState({data: data, form: form, files: [image]});
      }
    });

    if(!!this.props.match.params.id){
      EventApi.show(this.props.match.params.id);
    }
  }

  create(form, files){
    let file = files.length ? files[0] : null;

    if(!!this.props.match.params.id) {
      EventApi.edit(this.props.match.params.id, form.name, form.place, form.date, form.description, file, form.payment_fee, form.service_fee, form.repeated_fee);
    }else{
      EventApi.create(form.name, form.place, form.date, form.description, file, form.payment_fee, form.service_fee, form.repeated_fee);
    }
  }

  handleSubmit(e){
    e.preventDefault();

    let form = {};

    for(let field in this.state.form) {
      let dataValue = this.state.form[field];
      let transform = false;

      if(field in this.formData.fields) {
        transform = !!this.formData.fields[field].transform ? this.formData.fields[field].transform : false;
      }
      if (!!transform) {
        dataValue = transform(dataValue);
      }

      form[field] = dataValue;
    }

    this.create(form, this.state.files);
  }

  render() {
    let _self = this;

    if(!!this.state.created){
      return <Redirect to={AppConstants.routes.event.__INDEX_PATH}/>
    }

    return <div>
      {this.buildForm()}
    </div>
  }
}

export default withStyles(EventFormStyle)(EventFormComponent);
