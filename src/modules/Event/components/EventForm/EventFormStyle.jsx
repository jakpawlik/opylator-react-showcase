const EventFormStyle = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  icon: {
    marginLeft: theme.spacing.unit
  }
});

export default EventFormStyle;
