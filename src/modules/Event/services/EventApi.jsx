import API from '../../../services/API';
import EventActions from '../actions/EventActions';
import LoginActions from "../../Login/actions/LoginActions";
import axios from "axios";

class EventApi extends API {
  constructor(){
    super();
    let apiConfig = require('json-loader!yaml-loader!../cfg/api.yaml');
    this.api_url = apiConfig.api.api_url + '/' + apiConfig.api.asset_name + '/';
  }

  getIndex(url = this.api_url){
    return this.get(url).then(function(response) {
      let items = response.data._embedded.items;
      EventActions.dispatchIndex(items);
      return true;
    });
  }

  dashboard(){
    return this.get(this.api_url + 'dashboard').then(function(response) {
      EventActions.dispatchDashboard(response.data);
      return true;
    });
  }

  show(id, url = this.api_url){
    return this.get(url + id).then(function(response) {
      EventActions.dispatchData(response.data);
      return true;
    });
  }

  create(name, place, date, description, image, payment_fee, service_fee, repeated_fee){

    let config = this.config;

    config.headers['Content-Type'] = 'application/x-www-form-urlencoded';

    let promise = axios.post(this.api_url, {
      name, place, date, description, image, payment_fee, service_fee, repeated_fee
    }, this.config);

    return this.handleCreate(promise);
  }

  delete(event){
    let promise = axios.delete(this.getShowRoute({id: event.id}), this.config);
    let _this = this;

    return promise.then(function(response){
      _this.getIndex();
    });
  }

  edit(id, name, place, date, description, image, payment_fee, service_fee, repeated_fee){

    let config = this.config;

    config.headers['Content-Type'] = 'application/x-www-form-urlencoded';

    let promise = axios.put(this.getShowRoute({id: id}), {
      name, place, date, description, image, payment_fee, service_fee, repeated_fee
    }, this.config);

    return this.handleCreate(promise);
  }

  generate(n, price, pool, id){
    let promise = axios.post(this.api_url + 'generate-tickets', {
      n, price, pool, id
    }, this.config);

    return this.listTickets(promise);
  }

  listTickets(promise) {
    return promise
      .then(function(response) {
        EventActions.listTickets(response);
        return true;
      });
  }

  handleCreate(promise) {
    return promise
      .then(function(response) {
        EventActions.dispatchCreate(response);
        return true;
      });
  }

  getShowRoute(resource){
    return this.api_url + resource.id;
  }

  toggle(id){
    let _self = this;

    return this.get(this.api_url + 'toggle/' + id).then(function(response) {
      EventActions.dispatchToggle(response);
      _self.getIndex();
      return true;
    });
  }
}

export default new EventApi();
