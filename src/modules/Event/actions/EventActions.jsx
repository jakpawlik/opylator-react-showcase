import AppDispatcher from '../../../dispatchers/AppDispatcher';
import EventConstants from '../constants/EventConstants';

export default {
  changeStep(step){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_STEP_UPDATE,
      step: step
    });
  },
  dispatchIndex(list){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_LIST_UPDATE,
      list: list
    });
  },
  dispatchData(data){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_DATA_UPDATE,
      data: data
    });
  },
  dispatchCreate(data){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_DATA_CREATE,
      data: data
    });
  },
  listTickets(event){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_TICKET_GENERATED,
      tickets_generated: true,
      event: event
    });
  },
  openEventTicketsModal(event){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_LIST_MODAL,
      modal: true,
      eventId: !!event.id ? event.id : event
    });
  },
  closeEventTicketsModal(){
    AppDispatcher.dispatch({
      actionType: EventConstants.EVENT_LIST_MODAL,
      modal: false,
    });
  },
  dispatchDashboard(data){
    AppDispatcher.dispatch({
      actionType: EventConstants.DASHBOARD_DATA,
      data: data,
    });
  },
  dispatchToggle(data){
    AppDispatcher.dispatch({
      actionType: EventConstants.TOGGLE_EVENT_DATA,
      success: data.success,
    });
  }
}
