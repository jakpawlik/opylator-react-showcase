import Store from '../../../stores/Store';
import constants from '../constants/EventConstants';
import axios from 'axios';
import moment from "moment";
import IntlTools from "../../../services/IntlTools";

class EventStoreClass extends Store {

  constructor() {
    super();

    this.clear();
  }

  clear(){
    this.item = [];
    this.items = [];
    return this;
  }

  setCurrentItem(item){
    this.item = item;
    return this;
  }

  getCurrentItem(){
    return this.item;
  }

  processItems(items, countItems = true, availableItems = true, collections = false){

    for (let key in items) {
      
      let currentItem = items[key];

      if(!('place' in currentItem)){
        currentItem.place = '-';
      }

      if(!('description' in currentItem)){
        currentItem.description = '-';
      }

      if(availableItems) {
        currentItem.ticketPrice = currentItem.ticketsAvailableCount ? ('Od ' + currentItem.ticketsAvailable.first.price + ' PLN') : IntlTools.trans('app.ticket.sold_out');
      }

      if(!collections){
        delete currentItem.tickets;
        delete currentItem.ticketsAvailable;
      }
    }

    return items;
  }

  setItems(list, countItems = true, availableItems = true){
    this.items = this.processItems(list, countItems, availableItems);
    return this;
  }

  getItems(){
   return this.items;
  }

  getTransformers(){
    return {
      date: function(input){
        return moment(input).format('Y-MM-D HH:mm');
      }
    }
  }
}

const EventStore = new EventStoreClass();

EventStore.subscribe((payload) => {
  switch (payload.actionType) {
    case constants.EVENT_STEP_UPDATE:
      EventStore.emitChange({
        actionType: constants.EVENT_STEP_UPDATE,
        step: payload.step
      });
      break;

    case constants.EVENT_LIST_UPDATE:
        EventStore.setItems(payload.list);
        EventStore.emitChange({
          list_updated: true
        });
    break;

    case constants.EVENT_DATA_UPDATE:
        EventStore.setCurrentItem(payload.data);
        EventStore.emitChange({
          data_updated: true
        });
    break;

    case constants.EVENT_DATA_CREATE:
      EventStore.setCurrentItem(payload.data);
      EventStore.emitChange({
        data_created: true
      });
    break;

    case constants.EVENT_TICKET_GENERATED:
      EventStore.setCurrentItem(payload.event);
      EventStore.emitChange(payload);
      break;
    case constants.EVENT_LIST_MODAL:
      payload['availableTicket'] = null;

      if(!!payload.eventId) {
        axios.get('/public/ticket-available/' + payload.eventId).then(function (data) {
          payload['availableTicket'] = data.data;

          EventStore.emitChange(payload);
        });
      }else{
          EventStore.emitChange(payload);
      }
    break;

    case constants.DASHBOARD_DATA:
      EventStore.emitChange(payload);
      break;

    case constants.TOGGLE_EVENT_DATA:
      EventStore.emitChange(payload);
      break;
  }
});

export default EventStore;
