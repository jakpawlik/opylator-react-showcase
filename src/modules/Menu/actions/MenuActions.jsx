import AppDispatcher from '../../../dispatchers/AppDispatcher';
import MenuConstants from '../constants/MenuConstants';

export default {
  closeMenu(){
    AppDispatcher.dispatch({
      actionType: MenuConstants.MENU_CLOSE,
    });
  },
}
