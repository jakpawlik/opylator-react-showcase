import Store from '../../../stores/Store';
import constants from '../constants/MenuConstants';
import axios from 'axios';
import moment from "moment";

class MenuStoreClass extends Store {
  constructor() {
    super();
  }
}

const MenuStore = new MenuStoreClass();

MenuStore.subscribe((payload) => {
  switch (payload.actionType) {
    case constants.MENU_CLOSE:
        MenuStore.emitChange({
          close_menu: true
        });
    break;
  }
});

export default MenuStore;
