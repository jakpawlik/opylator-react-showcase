const menuStyle = theme => ({
  appBar: {
    top: "-30px",
    [theme.breakpoints.down("md")]: {
      top: "-15px"
    },
    backgroundColor: "transparent",
    boxShadow: "none",
    borderBottom: "0",
    marginBottom: "0",
    position: "absolute",
    width: "100%",
    paddingTop: "10px",
    zIndex: "1029",
    color: "#555555",
    border: "0",
    borderRadius: "3px",
    padding: "10px 0",
    transition: "all 150ms ease 0s",
    minHeight: "50px",
    display: "block"
  },
  hamburger: {
    position: "fixed",
    right: "30px",
    top: "30px"
  }
});

export default menuStyle;
