import React from 'react';

import DashboardComponent from '../../../components/Dashboard/Dashboard';
import TicketListComponent from '../../Ticket/components/TicketList/TicketList';
import EventListComponent from '../../Event/components/EventList/EventList';
import Sidebar from '../../../../vendor/material-dashboard-react/components/Sidebar/Sidebar';
import {
  Dashboard,
  SupervisorAccount,
  RemoveCircle, Menu
} from 'material-ui-icons';
import AppConstants from '../../../constants/AppConstants';
import {AppBar, Hidden, IconButton, withStyles} from "material-ui";

import menuStyle from "./menuStyle.jsx";
import cx from "classnames";
import MenuStore from '../stores/MenuStore';

const initialState = {
    elements: [],
    mobileOpen: false
};

const appRoutes = [
  {
    path: AppConstants.routes.__ROOT_PATH,
    sidebarName: 'Panel',
    navbarName: 'Panel sterowania',
    icon: Dashboard,
    component: DashboardComponent
  },
  {
    path: AppConstants.routes.event.__INDEX_PATH,
    sidebarName: 'Wydarzenia',
    navbarName: 'Lista wydarzeń',
    icon: SupervisorAccount,
    component: TicketListComponent
  },
  {
    path: AppConstants.routes.ticket.__INDEX_PATH,
    sidebarName: 'Bilety',
    navbarName: 'Lista ticketów',
    icon: SupervisorAccount,
    component: EventListComponent
  },
  {
    path: AppConstants.routes.__LOGOUT_PATH,
    sidebarName: 'Wyloguj się',
    navbarName: 'Wyloguj się',
    icon: RemoveCircle,
    component: DashboardComponent
  }
];

class MenuComponent extends React.Component {
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount(){
    let _self = this;

    MenuStore.addChangeListener((e) => {
      if(e.close_menu){
        _self.setState({mobileOpen: false});
      }
    });

  }

  render() {
    const { color, classes, ...rest } = this.props;

    let benc = require('../../../../../assets/img/logo.png');

    const appBarClasses = cx({
      [" " + classes[color]]: color
    });

    return (
      <nav>
        <AppBar className={classes.appBar + appBarClasses}>

          <Hidden mdUp>
            <IconButton
              className={ classes.hamburger }
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerToggle}
            >
              <Menu />
            </IconButton>
          </Hidden>
        </AppBar>

        <Sidebar
          routes={appRoutes}
          logoText={''}
          logo={benc}
          classes={ {img: 'img-responsive menu-logo', logo: 'drawer-menu-admin-logo'} }
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color='blue'
          {...rest}
        />
      </nav>
    );
  }
}

export default withStyles(menuStyle)(MenuComponent);
