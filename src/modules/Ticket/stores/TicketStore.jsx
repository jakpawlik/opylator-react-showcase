import Store from '../../../stores/Store';
import constants from '../constants/TicketConstants';
import QRCode from "qrcode.react";
import React from "react";
import AppConstants from "../../../constants/AppConstants";
import moment from 'moment';

class TicketStoreClass extends Store {

  constructor() {
    super();

    this.clear();
  }

  clear(){
    this.item = [];
    this.items = [];
    return this;
  }

  setCurrentItem(item){
    this.item = item;
    return this;
  }

  getCurrentItem(){
    return this.item;
  }

  setItems(list){
    let items = list;

    for (let key in items){
      //items[key].uid = <QRCode size={200} value={items[key].uid.toString()}  />
      if(!items[key].checked_at){
        items[key].checked_at = '-';
      }else{
        items[key].checked_at = moment(items[key].checked_at).format('YYYY-MM-D H:mm');
      }

      if(!items[key].sold_at){
        items[key].sold_at = '-';
      }else{
        items[key].sold_at = moment(items[key].sold_at).format('YYYY-MM-D H:mm');
      }

      items[key].sold = !!items[key].sold ? 'TAK' : ' NIE';
      items[key].checked = !!items[key].checked ? 'TAK' : ' NIE';
      items[key].price = 'od ' + items[key].price + ' PLN';

      if(items[key].checked_by) {
        delete items[key].checked_by;
      }
    }

    this.items = items;

    return this;
  }

  getItems(){
    return this.items;
  }
}

const TicketStore = new TicketStoreClass();

TicketStore.subscribe((payload) => {
  switch (payload.actionType) {
    case constants.TICKET_LIST_UPDATE:
        TicketStore.setItems(payload.list);
        TicketStore.emitChange({
          list_updated: true,
          sorted: payload.sorted,
          pageFinish: payload.pageFinish,
          page: payload.page
        });
    break;

    case constants.TICKET_LIST_CLEAR:
      TicketStore.clear();
      TicketStore.emitChange({
        list_clear: true,
      });
     break;

    case constants.TICKET_DATA_UPDATE:
        TicketStore.setCurrentItem(payload.data);
        TicketStore.emitChange({
          data_updated: true
        });
    break;

    case constants.TICKET_BUY:
      TicketStore.setCurrentItem(payload.ticket);
      TicketStore.emitChange({
        ticketSold: true,
        actionType: constants.TICKET_BUY
      });
    break;

    case AppConstants.SCANNER_ENABLE:
      TicketStore.emitChange({
        scannerEnabled: true,
        actionType: AppConstants.SCANNER_ENABLE
      });
    break;

    case AppConstants.SCANNER_DISABLE:
      TicketStore.emitChange({
        scannerEnabled: false,
        actionType: AppConstants.SCANNER_DISABLE
      });
    break;


    case constants.TICKET_CHECK:
      TicketStore.emitChange({
        ticketChecked: payload.success,
        ticketExists: payload.ticketExists,
        ticket: payload.ticket,
        actionType: constants.TICKET_CHECK
      });
    break;

    case constants.TICKET_EMAIL_CHECK:
      TicketStore.emitChange({
        emailsChecked: true,
        data: payload.data,
        actionType: constants.TICKET_EMAIL_CHECK
      });
     break;

    case constants.TICKET_EMAIL_SENT:
      TicketStore.emitChange({
        emailsSent: true,
        success: payload.success,
        actionType: constants.TICKET_EMAIL_SENT
      });
     break;

  }
});

export default TicketStore;
