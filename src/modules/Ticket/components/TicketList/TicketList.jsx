import React from 'react';
import AppConstants from '../../../../constants/AppConstants';

import {defineMessages} from 'react-intl';
import IntlComponent from '../../../../components/Intl/Intl';

import Table from '../../../../components/Table/Table';
import Link from '../../../../components/Link/Link';
import { withStyles } from 'material-ui';
import Button from 'muicss/lib/react/button';
import Input from 'muicss/lib/react/input';

import TicketListStyle from './TicketListStyle';
import TicketStore from '../../stores/TicketStore';
import TicketApi from '../../services/TicketApi';

import IntlTools from "../../../../services/IntlTools";
import InfiniteScroll from 'react-infinite-scroller';
import TicketActions from "../../actions/TicketActions";

const initialState = {
    items: [],
    paginationFinished: false,
    sorted: false,
    page: 1,
    searchId: ''
};

let messages = defineMessages({
  ticketListHeader: {
    id: 'app.ticket.name',
    defaultMessage: 'Tickets'
  },
  ticketListSubHeader: {
    id: 'app.ticket.uid',
    defaultMessage: 'Ticket list'
  }
});

class TicketListComponent extends IntlComponent {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentDidMount(){
    let _self = this;

    TicketStore.addChangeListener((e) => {

      if(e.list_clear){
        _self.setState({items: [], paginationFinished: false, page: 1});
      }

      if(e.list_updated){
          let items = this.state.items.concat(TicketStore.getItems());
          _self.setState({items: items, sorted: e.sorted, paginationFinished: e.pageFinish, page: e.page});
      }
    });

    if(!!this.props.match.params.id){
      TicketApi.getEventTicketsList(this.props.match.params.id, this.state.sorted, this.state.page);
    }else{
      TicketApi.getIndex(this.state.sorted, this.state.page);
    }
  }

  componentWillMount(){

    this.addMessages(messages);

    this.__header = {
      full_string: 'Id',
      pool: this.__f('app.event.pool'),
      price: this.__f('app.ticket.price'),
      payment_id: this.__f('app.ticket.payment_id'),
      email: this.__f('app.ticket.email'),
      sold: this.__f('app.ticket.sold'),
      sold_at: this.__f('app.ticket.soldAt'),
      checked: this.__f('app.ticket.checked_list'),
      checked_at: this.__f('app.ticket.checkedAt'),
      event: this.__f('app.ticket.event'),
  };

  }

  itemActions(element){
    let showRoute = AppConstants.routes.ticket.__INDEX_PATH + '/' + element.id;

    return <Link to={showRoute} text={"app.ticket.show"} />;
  }

  sort(){
    TicketActions.clearTickets();
    if(!!this.props.match.params.id){
      TicketApi.getEventTicketsList(this.props.match.params.id, true);
    }else{
      TicketApi.getIndex(true);
    }
  }

  list(){
    if(!!this.props.match.params.id){
      TicketApi.getEventTicketsList(this.props.match.params.id);
    }else{
      TicketApi.getIndex();
    }
  }

  changeSearch(e){
    let dataValue = e.target.value;

    this.setState({'searchId': dataValue });
  }

  search(){
    this.setState({'page': 1 });
    TicketActions.clearTickets();
    TicketApi.getEventTicketsList(this.props.match.params.id, this.state.sorted, this.state.page, this.state.searchId);
  }

  sortButton(){
    if(!!this.props.match.params.id){
      return <div>
        <Button color={'primary'} onClick={this.state.sorted ? this.list.bind(this) : this.sort.bind(this)}>{IntlTools.trans(this.state.sorted ? 'app.ticket.not_sold' : 'app.ticket.sold')}</Button>
        <div>
          <Input placeholder={'ID'} onChange={this.changeSearch.bind(this)} style={{width: 60, display: 'inline-block'}} value={this.state.searchId} />
          <Button color={'primary'} onClick={this.search.bind(this)} style={{display: 'inline-block'}}>{IntlTools.trans('app.ticket.search')}</Button>
        </div>
      </div>;
    }

    return null;
  }

  loadMore(){
    if(this.state.paginationFinished){
      return false;
    }

    if(!!this.props.match.params.id){
      TicketApi.getEventTicketsList(this.props.match.params.id, this.state.sorted, this.state.page + 1);
    }else{
      TicketApi.getIndex(this.state.sorted, this.state.page + 1);
    }
  }

  render() {
    return (
      <div>
        {this.sortButton()}
        {/*<Table elements={this.state.items} headers={this.__header} excluded={['created_by']} actions={this.itemActions} />*/}
        {/*<div style={{height:700, overflow: 'auto'}}>*/}
        <Table elements={this.state.items} headers={this.__header} excluded={['created_by']} actions={this.itemActions} />
        {!this.state.paginationFinished ? <Button color={'primary'} onClick={this.loadMore.bind(this)}>{IntlTools.trans('app.more')}</Button> : ''}
        {/*</div>*/}
      </div>
    );
  }
}

export default withStyles(TicketListStyle)(TicketListComponent.intl());
