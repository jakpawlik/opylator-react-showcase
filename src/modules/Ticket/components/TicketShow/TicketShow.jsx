import React from 'react';

import {defineMessages} from 'react-intl';
import IntlComponent from '../../../../components/Intl/Intl';

import { withStyles } from 'material-ui';

import TicketShowStyle from './TicketShowStyle';
import TicketStore from '../../stores/TicketStore';
import TicketApi from '../../services/TicketApi';

const initialState = {
    data: null
};

let messages = defineMessages({
  ticketShow: {
    id: 'app.ticket.show',
    defaultMessage: 'TicketShow data'
  }
});

class TicketShowComponent extends IntlComponent {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentWillMount(){
    this.addMessages(messages);
  }

  componentDidMount(){
    let _self = this;

    TicketStore.addChangeListener((e) => {
      if(e.data_updated){
        _self.setState({data: TicketStore.getCurrentItem()});
      }
    });
    
    TicketApi.show(this.props.match.params.id);
  }

  render(){
    return <div>
      {this.state.data ? <div><h1>{this.state.data.full_string}</h1><QRCode size={200} value={this.state.data.uid.toString()}  /></div> : null}
    </div>;
  }
}

export default withStyles(TicketShowStyle)(TicketShowComponent.intl());
