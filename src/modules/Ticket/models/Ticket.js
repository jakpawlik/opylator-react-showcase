import {attr, Model} from 'redux-orm';

class Ticket extends Model {
  toString() {
        return this.uid;
    }
}

Ticket.modelName = 'Ticket';

// Declare your related fields.
Ticket.fields = {
    id: attr(), // non-relational field for any value; optional but highly recommended
    uid: attr()
};

export default Ticket;
