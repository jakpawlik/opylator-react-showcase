import API from '../../../services/API';
import TicketActions from '../actions/TicketActions';
import axios from 'axios';
import DashboardActions from "../../../actions/DashboardActions";

class TicketApi extends API {
  constructor(){
    super();
    let apiConfig = require('json-loader!yaml-loader!../cfg/api.yaml');
    this.apiConfig = apiConfig;
    this.api_url = apiConfig.api.api_url + '/' + apiConfig.api.asset_name + '/';
  }

  getIndex(sort = false, page = 1){
    sort = sort ? '&sold=1' : '';

    return this.get(this.api_url + '?page='+ page + sort).then(function(response) {
      let items = response.data._embedded.items;
      for(let key in items) {
        items[key].event = !!items[key].event ? items[key].event.name : '';
      }

      TicketActions.dispatchIndex(items);
      return true;
    });
  }

  show(id, url = this.api_url){
    return this.get(url + id).then(function(response) {
      TicketActions.dispatchData(response.data);
      return true;
    });
  }

  buyTicket(ticket, payment){
    return axios.put('/public/buy-ticket', {ticket: ticket, payment: payment}).then(function(data){
      data = data.data;
      TicketActions.buyTicket(data.ticket, data.payment);
    });
  }

  checkTicket(uid) {
    return this.axios.post('/api/ticket/tickets/check', {uid: uid}).then(function (data) {
      TicketActions.checkTicket(data.data.success, data.data.ticketExists, data.data.ticket);
    });
  }

  getEventTicketsList(id, sort = false, p = 1, searchId = ''){
    sort = sort ? '1' : '0';

    let search = searchId === '' ? '' : '/' + searchId;

    return this.get(this.api_url + 'event/' + id + '/' + sort + search + '?page=' + p).then(function(response) {

      let items = response.data._embedded.items;

      for (let key in items) {
          items[key].event = items[key].event.name;
      }

      TicketActions.dispatchIndex(items, sort === '1', response.data.page === response.data.pages, response.data.page);

      return true;
    });
  }

  getEventTicketsListAll(id){
    return this.get(this.api_url + 'event/' + id + '/all').then(function(response) {

      let items = response.data;

      for (let key in items) {
        items[key].event = items[key].event.name;
      }

      TicketActions.dispatchIndex(items);

      return true;
    });
  }

  getEventTicketsListRoute(id){
    return this.api_url + '/event/' + id;
  }

  getShowRoute(resource){
    return this.api_url + resource.id;
  }

  getByEmails(emails, event){
    return this.post(this.api_url + 'check-emails', {emails, event}).then(function(response) {
      TicketActions.dispatchEmailData(response.data);
      return true;
    });
  }

  sendTickets(emails, event, normalize){
    DashboardActions.enableLoading();

    return this.post(this.api_url + 'send-emails', {emails, event, normalize}).then(function(e) {
      TicketActions.dispatchEmailSent(e.success);
      return true;
    });
  }
}

export default new TicketApi();
