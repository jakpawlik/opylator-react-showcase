import AppDispatcher from '../../../dispatchers/AppDispatcher';
import TicketConstants from '../constants/TicketConstants';
import EventConstants from "../../Event/constants/EventConstants";

export default {
  dispatchIndex(list, sorted = false, paginationFinished = false, page = 1){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_LIST_UPDATE,
      list: list,
      sorted: sorted,
      pageFinish: paginationFinished,
      page: page
    });
  },
  clearTickets(){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_LIST_CLEAR,
    });
  },
  dispatchData(data){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_DATA_UPDATE,
      data: data
    });
  },
  buyTicket(ticket, payment){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_BUY,
      ticket: ticket,
      payment: payment
    });
  },
  checkTicket(success, exists, ticket){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_CHECK,
      success: success,
      ticketExists: exists,
      ticket: ticket
    });
  },
  dispatchEmailData(data){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_EMAIL_CHECK,
      data: data,
    });
  },
  dispatchEmailSent(success){
    AppDispatcher.dispatch({
      actionType: TicketConstants.TICKET_EMAIL_SENT,
      success: success
    });
  }
}
