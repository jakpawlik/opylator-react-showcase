import React from 'react';
import { IntlProvider, addLocaleData } from 'react-intl';
import localeData from '../locales/messages';

import plLocaleData from 'react-intl/locale-data/pl';

class IntlToolsClass {
  constructor(){
    let _self = this;

    this._language = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;

    this._languageWithoutRegionCode = this._language.toLowerCase().split(/[_-]+/)[0];

    let lang = this.getLang();

    this.intlProvider = new IntlProvider({ locale: lang, messages: _self.getLocaleData() }, {});

    addLocaleData(plLocaleData);
  }

  addLocaleData(data){
    return addLocaleData(data);
  }

  getLocaleData(){
    return localeData[this.getLang()] || localeData.en;
  }

  getLang(){
    return this._languageWithoutRegionCode || this._language;
  }

  getIntl(){
    const { intl } = this.intlProvider.getChildContext();
    return intl;
  }

  trans(key){
    return this.getIntl().formatMessage({ id: key}, this.getLocaleData());
  }

  renderIntlProvider(contents){
    return <IntlProvider locale={this._languageWithoutRegionCode} messages={this.getLocaleData()}>
      {contents}
    </IntlProvider>
  }
}

const IntlTools = new IntlToolsClass();

Object.freeze(IntlTools);

export default IntlTools;
