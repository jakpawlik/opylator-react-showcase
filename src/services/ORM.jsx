import { ORM } from 'redux-orm';
import models  from '../../cfg/models/models';

const orm = new ORM();

for (let key in models){
  orm.register(models[key]);
}

export default orm;
