import axios from 'axios';
import LoginStore from '../modules/Login/stores/LoginStore';

class API {
  constructor(){
    this.axios = axios;

    this.config = {
      headers: {
        common: {
          'Authorization': 'Bearer ' + LoginStore.jwt
        },
        'Content-Type': 'application/json'
      }
    };
  }

  get(url){
    return this.axios.get(url, this.config);
  }

  post(url, data){
    return this.axios.post(url, data, this.config);
  }

  put(url, data){
    return this.axios.put(url, data, this.config);
  }
}

export default API;
