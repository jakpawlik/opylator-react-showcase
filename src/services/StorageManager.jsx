const USE_SESSION_STORAGE = true;

class StorageManagerClass {
  constructor(){
      this.storage = USE_SESSION_STORAGE ? sessionStorage : localStorage
  }

  getItem(key){
      return this.storage.getItem(key);
  }

  setItem(key,value){
      this.storage.setItem(key,value);
  }

  removeItem(key){
      this.storage.removeItem(key);
  }
}

const StorageManager = new StorageManagerClass();
Object.freeze(StorageManager);
export default StorageManager;
