import { createBrowserHistory } from 'history';

class HistoryClass {
  constructor(){
      this.history = createBrowserHistory();
  }
  push(path){
      this.history.push(path);
  }
}

const History = new HistoryClass();
Object.freeze(History);
export default History;
