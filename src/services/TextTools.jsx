import React from 'react';
import Table from '../../vendor/material-dashboard-react/components/Table/Table';
import IntlTools from './IntlTools';

class TextToolsClass {
  capitalize(s){
      return s.toLowerCase().replace( /\b./g, function(a){ return a.toUpperCase(); } );
  }
}

const TextTools = new TextToolsClass();
Object.freeze(TextTools);
export default TextTools;
