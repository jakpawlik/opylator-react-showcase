import Store from './Store';
import AppConstants from '../constants/AppConstants';

class DashboardStoreClass extends Store {
  constructor() {
    super();
  }
}

const DashboardStore = new DashboardStoreClass();

DashboardStore.subscribe((payload) => {
  switch (payload.actionType) {
    case AppConstants.LOADING:
        DashboardStore.emitChange({
          actionType: AppConstants.LOADING,
          loading: payload.loading
        });
    break;
  }
});



Object.freeze(DashboardStore);
export default DashboardStore;
