import { EventEmitter } from 'events';
import AppDispatcher from '../dispatchers/AppDispatcher';

export default class Store extends EventEmitter {

  constructor() {
    super();
  }

  subscribe(actionSubscribe) {
    this._dispatchToken = AppDispatcher.register(actionSubscribe);
  }

  get dispatchToken() {
    return this._dispatchToken;
  }

  emitChange(e) {
    this.emit('CHANGE', e);
  }

  addChangeListener(cb) {
    this.on('CHANGE', cb)
  }

  removeChangeListener(cb) {
    this.removeListener('CHANGE', cb);
  }

  addTransformers(data){
    let transformers = this.getTransformers();
    let newData = [];

    for(let index in data){
      let row = data[index];

      for(let key in row) {
        if (key in transformers) {
          let transformer = transformers[key];
          row[key] = transformer(row[key]);
        }
      }

      newData.push(row);
    }

    return newData;
  }
}
