import Store from './Store';
import AppConstants from '../constants/AppConstants';

class RedirectStoreClass extends Store {
  constructor() {
    super();
  }
}

const RedirectStore = new RedirectStoreClass();

RedirectStore.subscribe((payload) => {
  switch (payload.actionType) {
    case AppConstants.REDIRECT:
        RedirectStore.emitChange({
          redirect: true,
          route: payload.route
        });
    break;
  }
});



Object.freeze(RedirectStore);
export default RedirectStore;
