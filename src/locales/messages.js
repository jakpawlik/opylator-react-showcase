let offerLocale = JSON.parse(require('./event/messages.json'));
let ticketLocale = JSON.parse(require('./ticket/messages.json'));
let messages = JSON.parse(require('./appLocale.json'));

for (let locale in messages){
  Object.assign(messages[locale],
    offerLocale[locale],
    ticketLocale[locale]
  );
}

export default messages;
