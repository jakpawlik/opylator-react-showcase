import React from 'react';

import { withStyles } from 'material-ui';
import {withRouter} from "react-router";

import UserAppStyle from './UserAppStyle';
import {Route, Switch} from "react-router-dom";
import EventComponent from "./UserPart/Homepage/Event";
import HomepageComponent from "./UserPart/Homepage/Homepage";

require('normalize.css/normalize.css');

class UserApp extends React.Component {
  componentDidMount(){

    let _app = this;
  }

  constructor(props){
    super(props);
    this.state = {
      loading: false
    };
  }

  render() {
    return <Switch>
      <Route exact path='/' component={HomepageComponent} />
      <Route exact path='/event/:id' component={EventComponent} />
    </Switch>;
  }
}

UserApp.defaultProps = {};

export default withRouter(withStyles(UserAppStyle)(UserApp));
