import React from 'react';
import VendorTable from '../../../vendor/material-dashboard-react/components/Table/Table';
import IntlTools from '../../services/IntlTools';

class Table extends React.Component {

  constructor(props){
    super(props);
    this.fillTable();
  }

  componentWillReceiveProps(nextProps) {
    this.processData(nextProps.elements);
    this.setState({
      tableValues: this.tableValues
    });
  }

  processData(elements){
    let _this = this;
    _this.tableValues = [];

    elements.map((element) => {
      let rowValues = {};
      Object.keys(element).forEach(function(key) {
        let value = element[key];

        if(!_this.props.excluded.includes(key)) {
          let val = {};
          val[key] = value;

          rowValues = Object.assign(rowValues, val);
        }
      });

      if(Object.keys(element).length < _this.headerFields.length){
        let rewind = !!this.props.actions;
        for (let i = 0; i < _this.headerFields.length - Object.keys(element).length - rewind; i++) {
          let val = {};
          let key = Object.keys(element)[i];
          val[key] = '';
          rowValues = Object.assign(rowValues, val);
        }
      }

      if(this.props.actions !== false){
        let actions = _this.props.actions(element);
        let val = {};
        val['actions'] = actions;
        rowValues = Object.assign(rowValues, val);
      }

      _this.tableValues.push(rowValues);
    });
  }

  processHeaders(headers){
    let _this = this;
    _this.headerFieldsRaw = [];

    _this.headerCreated = headers.length;

    if(headers.length){
      _this.headerFields = this.props.headers;
      _this.headerCreated = true;
    }

    if(this.props.actions !== false){
      _this.headerFields = Object.assign(headers, {actions : ''});
    }
  }

  fillTable(){
    let _this = this;

    if(_this.props.elements.length || _this.props.forceAppear){
      _this.processHeaders(_this.props.headers);
      _this.processData(_this.props.elements);
    }

    this.state = {
      headerFields: _this.headerFields,
      tableValues: _this.tableValues
    }
  }

  render(){
    const {classes} = this.props;
    let tableClasses = {};

    for(let option in classes){
      tableClasses[option] = classes[option];
    }

    return <VendorTable
      classes={tableClasses}
      tableHeaderColor={this.props.color}
      tableHead={this.state.headerFields}
      tableData={this.state.tableValues}
    />
  }
}

Table.defaultProps = {
  elements: [],
  headers: [],
  actions: false,
  excluded: [],
  color: 'warning',
  forceAppear: true
};

export default Table;
