import React from 'react';
import RedirectActions from '../../actions/RedirectActions';
import Button from 'muicss/lib/react/button';
import IntlTools from '../../services/IntlTools';

class Link extends React.Component {
  constructor(props){
    super(props);
  }

  click(e){
    e.preventDefault();
    RedirectActions.redirect(this.props.to);
  }

  render(){
    return <Button color={this.props.color} className={this.props.linkClass} onClick={this.click.bind(this)}>{IntlTools.trans(this.props.text)}</Button>;
  }
}

Link.defaultProps = {
  linkClass: '',
  text: 'app.edit',
  color: 'primary'
};

export default Link;
