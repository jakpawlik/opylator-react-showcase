import React from 'react';
import moment from "moment";
import Modal from 'react-modal';

import EventStore from '../../../modules/Event/stores/EventStore';
import EventApi from '../../../modules/Event/services/EventApi';
import IntlTools from "../../../services/IntlTools";
import EventActions from "../../../modules/Event/actions/EventActions";
import EventConstants from "../../../modules/Event/constants/EventConstants";

import TicketApi from "../../../modules/Ticket/services/TicketApi";
import TicketStore from "../../../modules/Ticket/stores/TicketStore";
import TicketConstants from "../../../modules/Ticket/constants/TicketConstants";
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import Radio from 'muicss/lib/react/radio';

let logo = require('../../../../../assets/img/logo.png');

import HeaderComponent from './Header';
import EventsComponent from './Events';
import FooterComponent from './Footer';
import CookieComponent from './Cookie';

const initialState = {
  items: [],
  eventTickets: null,
  modalIsOpen: false,
  selectedEvent: null,
  selectedTicket: null,
  paymentType: 'p24',
  ticketBought: null,
  step: 1,
  form: {
    pay_email: '',
    pay_email2: '',
    qty: 1,
    reqCheck: false,
    marketingCheck: false
  }
};

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    width: '60%'
  }
};

const classes = {
  icon: {
    transform: 'translateY(7px)',
    marginLeft: 15,
    color: '#000'
  },
  checkboxText:{
    color: '#000',
    display: 'block',
    float: 'left',
    width: '90%',
    paddingTop: 15,
    fontSize: 10
  },
  inputMail: {
    margin: 'auto',
    padding: 10,
    width: '100%',
    maxWidth: 400
  },
  container: {
    background: '#010236',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: 940,
    maxWidth: '100%',
    margin: 'auto'
  }
};

Modal.setAppElement(document.getElementById('admin-app')?'#admin-app':'#app');

class EventComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  __f(txt){
    return IntlTools.trans(txt);
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.data_updated){
        _self.setState({selectedEvent: EventStore.getCurrentItem()});

      }

      if(e.actionType === EventConstants.EVENT_LIST_MODAL){
        _self.setState({selectedTicket: e.availableTicket});
      }

      if(e.actionType === EventConstants.EVENT_STEP_UPDATE){
        console.log(e);
        _self.setState({step: e.step});
      }
    });

    EventApi.show(this.props.match.params.id, '/public/');
    EventActions.openEventTicketsModal(this.props.match.params.id);
  }

  componentWillMount(){

  }

  onChange(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;

    if(fieldName === 'reqCheck' || fieldName === 'marketingCheck'){
      dataValue = e.target.checked;
    }

    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  checkOption(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;
    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  paymentType(e){
    this.setState({'paymentType': e.target.value });
  }

  onSubmit(e){
    e.preventDefault();
    document.location = '/public/pay-for-ticket/'+this.state.paymentType+'/' + this.state.selectedTicket.id + '?pay_email=' + this.state.form.pay_email + '&qty=' + this.state.form.qty + '&marketing=' + (this.state.form.marketingCheck ? '1' : '0');
  }

  addQty(){
    let val = this.state.form;
    let qty = this.state.form.qty;

    val['qty'] = qty + 1;

    this.setState({form: val});
  }

  subQty(){
    let val = this.state.form;
    let qty = this.state.form.qty;

    if(qty > 1){
      val['qty'] = qty - 1;
      this.setState({form: val});
    }
  }

  checkOpt(){
    let val = this.state.form;
    let opt = val['marketingCheck'];

    val['opt'] = !opt;
    this.setState({form: val});
  }

  checkReq(){
    let val = this.state.form;
    let opt = val['reqCheck'];

    val['opt'] = !opt;
    this.setState({form: val});
  }

  showPayments(){
    let element = this.state.selectedEvent;

    //<Button onClick={ function(){ document.location =  } color={'primary'}>{'Przejdź do strony płatności'}</Button>;
    return <div>{this.state.selectedTicket ? <Form onSubmit={ this.onSubmit.bind(this) }>
      {/*<label className={'mailLabel'}></label>*/}
      <div  className={'row'}>
        <div className={'col-sm-6'}>
          <Input style={classes.inputMail} placeholder="e-mail" data-field-name={'pay_email'} value={this.state.form.pay_email} onChange={this.onChange.bind(this)} type={'text'} name={'pay_email'} id={'pay_email'} />

          {/*<label className={'mailLabel'}></label>*/}
          <Input placeholder="potwierdź e-mail" style={classes.inputMail} data-field-name={'pay_email2'} value={this.state.form.pay_email2} onChange={this.onChange.bind(this)} type={'text'} name={'pay_email2'} id={'pay_email2'} />

          {this.state.form.pay_email.length && this.state.form.pay_email2.length && this.state.form.pay_email !== this.state.form.pay_email2 ? <label className={'mailLabel'} style={{color: '#900'}}>Adresy e-mail się nie zgadzają</label> : ''}

          <div className={'qty-container'}>
            <div className={'qty-label-holder'}>
              <div className={'qty-wrapper'}>
                <Input className={'qty-input'} data-field-name={'qty'} value={this.state.form.qty} onChange={this.onChange.bind(this)} type={'text'} name={'qty'} id={'qty'} />
                <div className={'qty-plus'} onClick={this.addQty.bind(this)}><div className={'chevron'}/></div>
                <div className={'qty-minus'} onClick={this.subQty.bind(this)}><div className={'chevron bottom'}/></div>
              </div>
              <label className={'mailLabel'} style={{color: '#000'}}>liczba biletów</label>
            </div>
          </div>

          <div style={{textAlign: 'center'}}>Koszt biletów: <strong>{this.state.selectedTicket.price * this.state.form.qty} PLN</strong></div>

          <div className={'checks'}>
            <div>
              <label className={this.state.form.marketingCheck?'checked':''} onClick={this.checkOpt.bind(this)} htmlFor={'opt'}>
              <Input id={'opt'} data-field-name={'marketingCheck'} type={'checkbox'} onClick={this.onChange.bind(this)} name={'marketingCheck'} />
              </label>
              <span style={classes.checkboxText}>
                (Opcjonalnie) Wyrażam zgodę na otrzymywanie od Benz Corp, zarejestrowana przy ul. Raciborskiej 160 w Rybniku pod numerem NIP 6423094767 oraz numerem REGON: 380459540 na podany przeze mnie adres e-mail informacji handlowych, w tym o promocjach, rabatach, wydarzeniach i ofertach.
              </span>
              <div style={{display: 'table', clear: 'both'}} />
            </div>

            <div>
              <label className={this.state.form.reqCheck?'checked':''} onClick={this.checkReq.bind(this)} htmlFor={'req'}>
              <Input id={'req'} data-field-name={'reqCheck'} type={'checkbox'} onClick={this.onChange.bind(this)} name={'reqCheck'} required/>
              </label>
              <span style={classes.checkboxText}>
                (Wymagane)  Oświadczam, że zapoznałem się z treścią <a href="https://opylator.pl/terms" target={'_blank'}>regulaminu</a> opylator.pl należącego do Benz Corp, zarejestrowana przy ul. Raciborskiej 160 w Rybniku pod numerem NIP 6423094767 oraz numerem REGON: 380459540 i akceptuję wszystkie jego postanowienia.
              </span>
              <div style={{display: 'table', clear: 'both'}} />
            </div>
          </div>
          {this.state.form.pay_email.length && this.state.form.reqCheck && this.state.form.pay_email === this.state.form.pay_email2 ?
            <div  style={{textAlign: 'center', marginTop: 20}}>
              {/*<strong style={{color: '#000'}}> Metoda płatności:</strong>*/}
              {/*<div>*/}
              {/*<Radio name="paymentType" value="p24" onClick={this.paymentType.bind(this)} label="Przelewy24" defaultChecked={true} />*/}
              {/*<Radio name="paymentType" value="paypal" label="PayPal" onClick={this.paymentType.bind(this)} />*/}
              {/*</div>*/}
              <button type={'submit'} className='main-button'>{IntlTools.trans('app.payment.process')}</button>
            </div>
            : ''}
        </div>
        <div className={'col-sm-6'}>
          <img src={this.state.selectedEvent.image} className={'img-responsive'}/>
        </div>
      </div>
    </Form> : ''}</div>;
  }

  changeStep(){
    EventActions.changeStep(2);
  }

  render() {
    let _self = this;

    return (
      <div className={'main_wrap'}>
        <HeaderComponent noNav={true}/>
        {this.state.selectedEvent ? <div id={'event'} className={'content-style'}>
          <h1>{moment(this.state.selectedEvent.date).format('d.MM.Y')} - {this.state.selectedEvent.name}</h1>
          {this.state.step === 1 ?
          <div className={'container'}>
            <div className={'row'}>
              <div className={'col-sm-6'} style={{marginTop: 0}}>
                <img src={this.state.selectedEvent.image} className={'img-responsive'}/>
              </div>
              <div className={'col-sm-6'}>
                <h2>{this.state.selectedEvent.place} / {moment(this.state.selectedEvent.date).format('HH:mm')}</h2>
                <h2>{this.state.selectedEvent.name}</h2>
                <p>
                  {this.state.selectedEvent.description}
                </p>

                <span onClick={_self.changeStep.bind(this)} className={'main-button'}>Przejdź do zakupu</span>
              </div>
            </div>
          </div>:null}
          {(this.state.selectedEvent && this.state.step === 2 ? <div className="container">{_self.showPayments()}</div> : '')}
        </div> : ''}

        <FooterComponent/>
        <CookieComponent/>
      </div>
    );
  }
}

export default EventComponent;
