import React from 'react';
import Table from "../../Table/Table";
import IntlTools from "../../../services/IntlTools";

import OwlCarousel from 'react-owl-carousel2';

const sliderOptions = {
  items: 1,
  nav: false,
  rewind: true,
  dots: true
};

class EventsComponent extends React.Component {

  constructor(props){
    super(props);
  }

  printItems(){
    let items = [];

    for(let key in this.props.items){
      let item = this.props.items[key];

      items.push(<article key={key} className={'event-item'}>
        <img src={item.image}/>
        <h2 className={'nolink'}><div className={'main-button'}>Od {item.ticket_price} PLN</div></h2>
        <div className={'event-button'}>
          {item.tickets_available_count > 0 ? <div className={'main-button'} onClick={() => this.props.openModal(item)}>{IntlTools.trans('app.event.buyTicket')}</div> : <span className={'main-button disabled-button'}>{IntlTools.trans('app.event.noTickets')}</span>}
        </div>
      </article>);
    }

    return <OwlCarousel ref="car" options={sliderOptions} >{items.map((elem, i) => {
      return elem;
    })}</OwlCarousel>;
  }

  render() {
    return this.printItems();
  }
}

EventsComponent.defaultProps = {
  items: [],
  openModal: function(){}
};

export default EventsComponent;
