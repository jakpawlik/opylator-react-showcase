import React from 'react';


class FooterComponent extends React.Component {
  render() {
    return <footer>

      {/*<div className={'rodoNotice'}>*/}
        {/*Administratorem Twoich danych osobowych, czyli podmiotem decydującym o celach i sposobach ich przetwarzania jest Marcin Śliwiński, zarejestrowana przy ul. Daszyńskiego 480D w Gliwicach pod numerem NIP 6312639695 oraz numerem REGON: 382090547.*/}
        {/*Twoje dane osobowe będą przetwarzane zgodnie z prawem, przede wszystkim z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) o Ochronie Danych Osobowych (RODO), w celu wykonania zawartej z Tobą umowy oraz w celach wskazanych w treści Twoich zgód (Twoje zgody są dobrowolne), przy zachowaniu Twoich praw tj.*/}
        {/*prawa wycofania zgody na przetwarzanie danych osobowych, prawa dostępu, sprostowania oraz usunięcia Twoich danych, ograniczenia ich przetwarzania, prawa do ich przenoszenia, prawa do niepodlegania zautomatyzowanemu podejmowaniu decyzji, w tym profilowaniu, a także prawa wyrażenia sprzeciwu wobec przetwarzania Twoich danych osobowych.*/}
        {/*W razie jakichkolwiek Twoich pytań, czy wątpliwości jesteśmy dostępni pod adresem e-mail info@opylator.pl*/}
      {/*</div>*/}
    </footer>;
  }
}

export default FooterComponent;
