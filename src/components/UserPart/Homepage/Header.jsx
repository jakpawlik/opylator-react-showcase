import React from 'react';

let logo = require('../../../../../assets/img/logo.png');
const fbicon = require('../../../../../assets/img/fb.png');
const insta = require('../../../../../assets/img/insta.png');

class HeaderComponent extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (<header className={'main_header'}>
      <div className={'container flex flex-row flex-explode header-container'}>
        <a href={'/'} className={'main_logo'}>
          <img src={logo} />
        </a>
        {/*<a className={'insta'} target="_blank" href={'#'}>*/}
          {/*<img src={insta} />*/}
        {/*</a>*/}
      </div>
      {!this.props.noNav ? <nav>
        <ul>
          <li>
            <a href={'/'}>Wydarzenia</a>
          </li>
          <li>
            <a href={'/privacy-policy'}>Polityka prywatności</a>
          </li>
          <li>
            <a href={'/terms'}>Regulamin</a>
          </li>
          <li>
            <a className={'fb'} target="_blank" href={'http://facebook.com/opylator'}>
              <img src={fbicon} />
            </a>
          </li>
          {/*<li>*/}
            {/*<a href={'#'}>Kontakt</a>*/}
          {/*</li>*/}
        </ul>
      </nav> : ''}
      {/*<p style={{color: '#F00', textAlign: 'center', fontWeight: 'bold'}}>*/}
        {/*Trwają prace nad systemem transakcyjnym. Prosimy o cierpliwość.*/}
      {/*</p>*/}
    </header>);
  }
}

HeaderComponent.defaultProps = {
  noNav: false
};

export default HeaderComponent;
