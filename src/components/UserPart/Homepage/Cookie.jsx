import CookieConsent from "react-cookie-consent";
import React from "react";

class CookieComponent extends React.Component {
  render(){
    return <CookieConsent
      location="bottom"
      buttonText="Zgadzam się"
      cookieName="cookiecheck_pb"
      style={{ background: "#202020" }}
      buttonStyle={{ backgroundColor: '#d7bc5f', color: "#FFF", fontSize: "13px", padding: 10, border: '1px solid #d7bc5f' }}
      expires={150}
    >
      Używamy plików Cookies, aby zapewnić naszym Użytkownikom wygodne korzystanie z naszej strony internetowej oraz aby stale poprawiać jej jakość. Poprzez odpowiednie ustawienia swojej przeglądarki Użytkownik wyraża zgodę na stosowanie określonych Cookies. Użytkownik może w każdej chwili zmienić ustawienia przeglądarki, dostosowując je do swoich oczekiwań. Zmiana ustawień i cofnięcie zgód na stosowanie Cookies nie wpływa jednak na zgodność z prawem ich stosowania przed dokonaniem zmiany. Szczegółowe informacje znajdziesz w Polityce prywatności dostępnej na stronie https://opylator.pl/privacy-policy.
    </CookieConsent>
  }
}

export default CookieComponent;
