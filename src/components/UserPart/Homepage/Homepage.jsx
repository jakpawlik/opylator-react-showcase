import React from 'react';


import Modal from 'react-modal';

import EventStore from '../../../modules/Event/stores/EventStore';
import EventApi from '../../../modules/Event/services/EventApi';
import IntlTools from "../../../services/IntlTools";
import EventActions from "../../../modules/Event/actions/EventActions";
import EventConstants from "../../../modules/Event/constants/EventConstants";

import TicketApi from "../../../modules/Ticket/services/TicketApi";
import TicketStore from "../../../modules/Ticket/stores/TicketStore";
import TicketConstants from "../../../modules/Ticket/constants/TicketConstants";
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import Radio from 'muicss/lib/react/radio';

let logo = require('../../../../../assets/img/logo.png');

import HeaderComponent from './Header';
import EventsComponent from './Events';
import FooterComponent from './Footer';
import CookieComponent from './Cookie';
import Link from "../../Link/Link";
import AppConstants from "../../../constants/AppConstants";
import RedirectActions from "../../../actions/RedirectActions";

const initialState = {
    items: [],
    eventTickets: null,
    modalIsOpen: false,
    selectedEvent: null,
    selectedTicket: null,
    paymentType: 'p24',
    ticketBought: null,
    form: {
      pay_email: '',
      pay_email2: '',
      qty: 1,
      reqCheck: false,
      marketingCheck: false
    }
};

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    width: '60%'
  }
};

const classes = {
  icon: {
    transform: 'translateY(7px)',
    marginLeft: 15,
    color: '#000'
  },
  checkboxText:{
    color: '#000',
    display: 'block',
    float: 'left',
    width: '90%',
    paddingTop: 15,
    fontSize: 10
  },
  inputMail: {
    margin: 'auto',
    padding: 10,
    width: 400,
    maxWidth: '100%'
  },
  container: {
    background: '#010236',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: 940,
    maxWidth: '100%',
    margin: 'auto'
  }
};

Modal.setAppElement(document.getElementById('admin-app')?'#admin-app':'#app');

class HomepageComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  __f(txt){
    return IntlTools.trans(txt);
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.list_updated){
        let items = EventStore.getItems();
        _self.setState({items: items});
      }

      if(e.actionType === EventConstants.EVENT_LIST_MODAL){
        _self.setState({selectedEvent: e.event, selectedTicket: e.availableTicket});
      }
    });

    TicketStore.addChangeListener((e) => {
      if(e.actionType === TicketConstants.TICKET_BUY){
        _self.setState({ticketBought: TicketStore.getCurrentItem(), selectedTicket: null});
      }
    });

    EventApi.getIndex('/public');
  }

  componentWillMount(){
    this.__header = {
      name: this.__f('app.event.name'),
      description: this.__f('app.event.description'),
      date: this.__f('app.event.date'),
      place: this.__f('app.event.place'),
      ticketPrice: this.__f('app.ticket.price'),
    };
  }

  itemActions(element) {


    return <div>
      {element.ticketsAvailableCount > 0 ? <div className={'main-button'} onClick={() => this.openModal(element)}>{this.__f('app.event.buyTicket')}</div> : null} | <a href={'/'}>więcej info ></a>
    </div>;
  }

  openModal(element){
    let showRoute = AppConstants.routes.event.__PUBLIC + element.id;
    RedirectActions.redirect(showRoute);
  }

  afterOpenModal(){
  }

  closeModal(){
    EventActions.closeEventTicketsModal();
  }

  onSuccess(payment){
    TicketApi.buyTicket(this.state.selectedTicket, payment);
    EventApi.getIndex('/public');
  }

  onChange(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;

    if(fieldName === 'reqCheck' || fieldName === 'marketingCheck'){
      dataValue = e.target.checked;
    }

    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  checkOption(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;
    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  paymentType(e){
    this.setState({'paymentType': e.target.value });
  }

  onSubmit(e){
    e.preventDefault();
    document.location = '/public/pay-for-ticket/'+this.state.paymentType+'/' + this.state.selectedTicket.id + '?pay_email=' + this.state.form.pay_email + '&qty=' + this.state.form.qty + '&marketing=' + (this.state.form.marketingCheck ? '1' : '0');
  }

  showPayments(){
    let element = this.state.selectedEvent;
    //<Button onClick={ function(){ document.location =  } color={'primary'}>{'Przejdź do strony płatności'}</Button>;
    return <div>{this.state.selectedTicket ? <Form onSubmit={ this.onSubmit.bind(this) }>
      <label className={'mailLabel'}>Podaj swój adres e-mail</label>
      <Input style={classes.inputMail} data-field-name={'pay_email'} value={this.state.form.pay_email} onChange={this.onChange.bind(this)} type={'text'} name={'pay_email'} id={'pay_email'} />

      <label className={'mailLabel'}>Potwierdź swój adres e-mail</label>
      <Input style={classes.inputMail} data-field-name={'pay_email2'} value={this.state.form.pay_email2} onChange={this.onChange.bind(this)} type={'text'} name={'pay_email2'} id={'pay_email2'} />

      {this.state.form.pay_email.length && this.state.form.pay_email2.length && this.state.form.pay_email !== this.state.form.pay_email2 ? <label className={'mailLabel'} style={{color: '#900'}}>Adresy e-mail się nie zgadzają</label> : ''}

      <label className={'mailLabel'} style={{color: '#000'}}>Liczba biletów</label>
      <Input style={classes.inputMail} data-field-name={'qty'} value={this.state.form.qty} onChange={this.onChange.bind(this)} type={'text'} name={'qty'} id={'qty'} />

      <div className={'checks'}>
        <div>
          <Input data-field-name={'marketingCheck'} type={'checkbox'} onClick={this.onChange.bind(this)} name={'marketingCheck'} />
          <span style={classes.checkboxText}>
            (Opcjonalnie) Wyrażam zgodę na otrzymywanie od Benz Corp, zarejestrowana przy ul. Raciborskiej 160 w Rybniku pod numerem NIP 6423094767 oraz numerem REGON: 380459540 na podany przeze mnie adres e-mail informacji handlowych, w tym o promocjach, rabatach, wydarzeniach i ofertach.
          </span>
          <div style={{display: 'table', clear: 'both'}} />
        </div>

        <hr/>

        <div>
          <Input data-field-name={'reqCheck'} type={'checkbox'} onClick={this.onChange.bind(this)} name={'reqCheck'} required/>
          <span style={classes.checkboxText}>
            (Wymagane)  Oświadczam, że zapoznałem się z treścią <a href="https://opylator.pl/terms" target={'_blank'}>regulaminu</a> opylator.pl należącego do Benz Corp, zarejestrowana przy ul. Raciborskiej 160 w Rybniku pod numerem NIP 6423094767 oraz numerem REGON: 380459540 i akceptuję wszystkie jego postanowienia.
          </span>
          <div style={{display: 'table', clear: 'both'}} />
        </div>
      </div>
      {this.state.form.pay_email.length && this.state.form.reqCheck && this.state.form.pay_email === this.state.form.pay_email2 ?
        <div style={{textAlign: 'center', marginTop: 20}}>
          {/*<strong style={{color: '#000'}}> Metoda płatności:</strong>*/}
          {/*<div>*/}
            {/*<Radio name="paymentType" value="p24" onClick={this.paymentType.bind(this)} label="Przelewy24" defaultChecked={true} />*/}
            {/*<Radio name="paymentType" value="paypal" label="PayPal" onClick={this.paymentType.bind(this)} />*/}
          {/*</div>*/}
          <button type={'submit'} className='main-button'>{IntlTools.trans('app.payment.process')}</button>
        </div>
        : ''}
    </Form> : ''}</div>;
  }
//<h1 style={{color: '#d7bc5f', textAlign: 'center'}}>Przerwa techniczna do 01.02.2019r.</h1>
  render() {
    let _self = this;

    return (<div className={'main_wrap'}>
        <HeaderComponent />
        <div className={''}>
          <EventsComponent openModal={this.openModal.bind(this)} items={this.state.items} />
        </div>
        <FooterComponent />
        <CookieComponent />
      </div>);
  }
}

export default HomepageComponent;
