import React from 'react';
import Table from '../Table/Table';

import PropTypes from 'prop-types';

import { withStyles, Grid, Card, CardHeader, Avatar } from 'material-ui';
import dashboardStyle from '../../../vendor/material-dashboard-react/variables/styles/dashboardStyle';

import {defineMessages, injectIntl} from 'react-intl';

import IntlTools from '../../services/IntlTools';
import EventStore from "../../modules/Event/stores/EventStore";
import EventConstants from "../../modules/Event/constants/EventConstants";
import EventApi from "../../modules/Event/services/EventApi";

const initialState = {
  percentage: 0
};

class DashboardComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentDidMount(){
    let _self = this;
    EventStore.addChangeListener((e) => {
      if(e.actionType === EventConstants.DASHBOARD_DATA){
        console.log('percentage');
        _self.setState('percentage', e.data.percentage);
      }
    });

    EventApi.dashboard();
  }

  render() {
    let title = IntlTools.trans('app.sold_percentage');
    return (
      <div className="container">
        <Card>
          <CardHeader
            avatar={
              <Avatar aria-label="Front">
                {this.state.percentage + '%'}
              </Avatar>
            }
            title={ title }
          />
        </Card>
      </div>
    );
  }
}

DashboardComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(injectIntl(DashboardComponent));
