let butt = require('../../../../../assets/svg/btn.svg');

const EventUserListStyle = theme => ({
  icon: {
    transform: 'translateY(7px)',
    marginLeft: theme.spacing.unit,
    color: '#000'
  },
  imgWrap: {
    padding: '10px',
    marginTop: 25
  },
  img: {
    maxWidth: '100%',
    display: 'block',
    margin: 'auto'
  },
  table: {
    marginTop: 25,
    border: '8px solid #767676',
    background: '#FFF'
  },
  tableCell: {
    color: '#000',
    border: 'none',
    fontWeight: 'normal'
  },
  tableHeadRow: {
    borderBottom: '5px solid #767676'
  },
  tableRow: {

  },
  tableHeadCell: {
    color: '#000',
    fontWeight: 'bold'
  },
  checkboxText:{
    color: '#000',
    display: 'block',
    float: 'left',
    width: '90%',
    paddingTop: 15
  },
  inputMail: {
    margin: 'auto',
    padding: 10,
    width: 400,
    maxWidth: '100%'
  },
  container: {
    background: '#010236',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: 940,
    maxWidth: '100%',
    margin: 'auto'
  }
});

export default EventUserListStyle;
