import React from 'react';


import Table from '../../../components/Table/Table';
import {Card, CardHeader, CardContent,  Typography, Divider, withStyles} from 'material-ui';
import Modal from 'react-modal';

import EventUserListStyle from './EventUserListStyle';
import EventStore from '../../../modules/Event/stores/EventStore';
import EventApi from '../../../modules/Event/services/EventApi';
import IntlTools from "../../../services/IntlTools";
import EventActions from "../../../modules/Event/actions/EventActions";
import EventConstants from "../../../modules/Event/constants/EventConstants";

import TicketApi from "../../../modules/Ticket/services/TicketApi";
import TicketStore from "../../../modules/Ticket/stores/TicketStore";
import TicketConstants from "../../../modules/Ticket/constants/TicketConstants";
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import Radio from 'muicss/lib/react/radio';
import CookieConsent from "react-cookie-consent";

let logo = require('../../../../../assets/img/logo.png');
let butt = require('../../../../../assets/svg/btn.svg');

import {
  Payment as PaymentIcon,
} from 'material-ui-icons';

const initialState = {
    items: [],
    eventTickets: null,
    modalIsOpen: false,
    selectedEvent: null,
    selectedTicket: null,
    paymentType: 'p24',
    ticketBought: null,
    form: {
      pay_email: '',
      pay_email2: '',
      qty: 1,
      reqCheck: false,
      marketingCheck: false
    }
};

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    width: '60%'
  }
};

Modal.setAppElement(document.getElementById('admin-app')?'#admin-app':'#app');

class EventUserListComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  __f(txt){
    return IntlTools.trans(txt);
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.list_updated){
        let items = EventStore.getItems();
        _self.setState({items: items});
      }

      if(e.actionType === EventConstants.EVENT_LIST_MODAL){
        _self.setState({modalIsOpen: e.modal, selectedEvent: e.event, selectedTicket: e.availableTicket});
      }
    });

    TicketStore.addChangeListener((e) => {
      if(e.actionType === TicketConstants.TICKET_BUY){
        _self.setState({ticketBought: TicketStore.getCurrentItem(), selectedTicket: null});
      }
    });

    EventApi.getIndex('/public');
  }

  componentWillMount(){
    this.__header = {
      name: this.__f('app.event.name'),
      description: this.__f('app.event.description'),
      date: this.__f('app.event.date'),
      place: this.__f('app.event.place'),
      ticketPrice: this.__f('app.ticket.price'),
    };
  }

  itemActions(element) {
    let {classes} = this.props;
    return <div>
      {element.ticketsAvailableCount > 0 ? <Button color='primary' className={'mui-btn mui-btn--primary main-button'} onClick={() => this.openModal(element)}>{this.__f('app.event.buyTicket')}</Button> : null}
    </div>;
  }

  openModal(element){
    EventActions.openEventTicketsModal(element);
  }

  afterOpenModal(){
  }

  closeModal(){
    EventActions.closeEventTicketsModal();
  }

  onSuccess(payment){
    TicketApi.buyTicket(this.state.selectedTicket, payment);
    EventApi.getIndex('/public');
  }

  onChange(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;

    if(fieldName === 'reqCheck' || fieldName === 'marketingCheck'){
      dataValue = e.target.checked;
    }

    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  checkOption(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;
    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  paymentType(e){
    this.setState({'paymentType': e.target.value });
  }

  onSubmit(e){
    e.preventDefault();
    document.location = '/public/pay-for-ticket/'+this.state.paymentType+'/' + this.state.selectedTicket.id + '?pay_email=' + this.state.form.pay_email + '&qty=' + this.state.form.qty + '&marketing=' + (this.state.form.marketingCheck ? '1' : '0');
  }

  showPayments(){
    let {classes} = this.props;

    let element = this.state.selectedEvent;
    //<Button onClick={ function(){ document.location =  } color={'primary'}>{'Przejdź do strony płatności'}</Button>;
    return <div>{this.state.selectedTicket ? <Form onSubmit={ this.onSubmit.bind(this) }>
      <label className={'mailLabel'}>Podaj swój adres e-mail</label>
      <Input className={classes.inputMail} data-field-name={'pay_email'} value={this.state.form.pay_email} onChange={this.onChange.bind(this)} type={'text'} name={'pay_email'} id={'pay_email'} />

      <label className={'mailLabel'}>Potwierdź swój adres e-mail</label>
      <Input className={classes.inputMail} data-field-name={'pay_email2'} value={this.state.form.pay_email2} onChange={this.onChange.bind(this)} type={'text'} name={'pay_email2'} id={'pay_email2'} />

      {this.state.form.pay_email.length && this.state.form.pay_email2.length && this.state.form.pay_email !== this.state.form.pay_email2 ? <label className={'mailLabel'} style={{color: '#900'}}>Adresy e-mail się nie zgadzają</label> : ''}

      <label className={'mailLabel'} style={{color: '#000'}}>Liczba biletów</label>
      <Input data-field-name={'qty'} value={this.state.form.qty} onChange={this.onChange.bind(this)} type={'text'} name={'qty'} id={'qty'} />

      <div className={'checks'}>
        <p>
          <Input data-field-name={'marketingCheck'} type={'checkbox'} onClick={this.onChange.bind(this)} name={'marketingCheck'} />
          <span className={classes.checkboxText}>
            (Opcjonalnie) Wyrażam zgodę na otrzymywanie od Marcin Śliwiński, zarejestrowana przy ul. Daszyńskiego 480D w Gliwicach pod numerem NIP 6312639695 oraz numerem REGON: 382090547 na podany przeze mnie adres e-mail informacji handlowych, w tym o promocjach, rabatach, wydarzeniach i ofertach.
          </span>
        </p>

        <hr/>

        <p>
          <Input data-field-name={'reqCheck'} type={'checkbox'} onClick={this.onChange.bind(this)} name={'reqCheck'} required/>
          <span className={classes.checkboxText}>
            (Wymagane)  Oświadczam, że zapoznałem się z treścią <a href="https://opylator.pl/terms" target={'_blank'}>regulaminu</a> opylator.pl należącego do Marcin Śliwiński, zarejestrowana przy ul. Daszyńskiego 480D w Gliwicach pod numerem NIP 6312639695 oraz numerem REGON: 382090547 i akceptuję wszystkie jego postanowienia.
          </span>
        </p>
      </div>
      {this.state.form.pay_email.length && this.state.form.reqCheck && this.state.form.pay_email === this.state.form.pay_email2 ?
        <div>
          {/*<strong style={{color: '#000'}}> Metoda płatności:</strong>*/}
          {/*<div>*/}
            {/*<Radio name="paymentType" value="p24" onClick={this.paymentType.bind(this)} label="Przelewy24" defaultChecked={true} />*/}
            {/*<Radio name="paymentType" value="paypal" label="PayPal" onClick={this.paymentType.bind(this)} />*/}
          {/*</div>*/}
          <Button color='primary'>{IntlTools.trans('app.payment.process')}</Button>
        </div>
        : ''}
    </Form> : ''}</div>;


  }

  displayMessages(){
    if(this.state.loading){
      return <Card>
        <CardHeader
          title={ this.__f('app.payment.in_progress') }
        />
        <CardContent>
          <Typography component="h3" variant="headline" color="textSecondary" gutterBottom>
            {this.__f('app.payment.processing_payment')}
          </Typography>
        </CardContent>
      </Card>;
    }

    if(this.state.ticketBought) {
      return <Card>
        <CardHeader
          title={ this.__f('app.payment.done') }
        />
        <CardContent>
          <Typography component="h3" variant="headline" color="textSecondary" gutterBottom>
            {this.state.ticketBought.event.name}
          </Typography>
          <Divider />
          <Typography component="h4" variant="subheadline" color="textSecondary" gutterBottom>
            {this.__f('app.payment.id')}: {this.state.ticketBought.paymentId}
          </Typography>
          <Button color={'primary'} onClick={this.closeModal.bind(this)}>{this.__f('app.hide.buy_modal')}</Button>
        </CardContent>
      </Card>
    }
  }

  render() {
    const {classes} = this.props;
    let _self = this;

    let tableClasses = {};

    for(let option in classes){
      tableClasses[option] = classes[option];
    }

    delete tableClasses['button'];
    delete tableClasses['icon'];
    delete tableClasses['imgWrap'];
    delete tableClasses['img'];
    delete tableClasses['container'];
    delete tableClasses['checkboxText'];


    return (
      <div className={classes.container}>
        {/*<h1 style={{textAlign: 'center', color: '#FFF'}}>*/}
          {/*Dosiego roku życzy zespół Besbiletoo.*/}
          {/*<br />*/}
          {/*Wracamy 04.01.2019 r. o godz. 20:00*/}
        {/*</h1>*/}
        <div className={classes.imgWrap}>
          <img src={logo} className={classes.img} />
        </div>
        <Table classes={tableClasses} elements={EventStore.addTransformers(this.state.items)} headers={this.__header} actions={this.itemActions.bind(this)} excluded={['tickets', 'ticketsAvailable']} />
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel={this.__f('app.event.payment')}
          style={customStyles}
        >
          {this.displayMessages()}
          {(!this.state.loading && this.state.selectedEvent ? _self.showPayments() : '')}
        </Modal>
        <CookieConsent
          location="bottom"
          buttonText="Zgadzam się"
          cookieName="cookiecheck_pb"
          style={{ background: "#2B373B" }}
          buttonStyle={{ backgroundColor: '#030030', color: "#FFF", fontSize: "13px", padding: 10, border: '1px solid #FFF' }}
          expires={150}
        >
          Używamy plików Cookies, aby zapewnić naszym Użytkownikom wygodne korzystanie z naszej strony internetowej oraz aby stale poprawiać jej jakość. Poprzez odpowiednie ustawienia swojej przeglądarki Użytkownik wyraża zgodę na stosowanie określonych Cookies. Użytkownik może w każdej chwili zmienić ustawienia przeglądarki, dostosowując je do swoich oczekiwań. Zmiana ustawień i cofnięcie zgód na stosowanie Cookies nie wpływa jednak na zgodność z prawem ich stosowania przed dokonaniem zmiany. Szczegółowe informacje znajdziesz w Polityce prywatności dostępnej na stronie https://opylator.pl/privacy-policy.
        </CookieConsent>
        {/*<div className={'rodoNotice'}>*/}
          {/*Administratorem Twoich danych osobowych, czyli podmiotem decydującym o celach i sposobach ich przetwarzania jest Marcin Śliwiński, zarejestrowana przy ul. Daszyńskiego 480D w Gliwicach pod numerem NIP 6312639695 oraz numerem REGON: 382090547.  Twoje dane osobowe będą przetwarzane zgodnie z prawem, przede wszystkim z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) o Ochronie Danych Osobowych (RODO), w celu wykonania zawartej z Tobą umowy oraz w celach wskazanych w treści Twoich zgód (Twoje zgody są dobrowolne), przy zachowaniu Twoich praw tj. prawa wycofania zgody na przetwarzanie danych osobowych, prawa dostępu, sprostowania oraz usunięcia Twoich danych, ograniczenia ich przetwarzania, prawa do ich przenoszenia, prawa do niepodlegania zautomatyzowanemu podejmowaniu decyzji, w tym profilowaniu, a także prawa wyrażenia sprzeciwu wobec przetwarzania Twoich danych osobowych. W razie jakichkolwiek Twoich pytań, czy wątpliwości jesteśmy dostępni pod adresem e-mail info@opylator.pl*/}
        {/*</div>*/}
      </div>
    );
  }
}

export default withStyles(EventUserListStyle)(EventUserListComponent);
