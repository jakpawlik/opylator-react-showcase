import React from 'react';

import {injectIntl} from 'react-intl';


class IntlComponent extends React.Component {
  constructor(props){
    super(props);
    this.messages = null;
  }

  addMessages(messages){
    this.messages = messages;
    return this;
  }

  static intl(){
    return injectIntl(this);
  }

  __f(key){
    return this.props.intl.formatMessage({ id : key}, this.messages);
  }
}

export default IntlComponent;
