import MenuStore from "../modules/Menu/stores/MenuStore";

require('normalize.css/normalize.css');
import React from 'react';

import {
  Switch,
  Route
} from 'react-router-dom';

import { withStyles } from 'material-ui';

import { withRouter } from 'react-router'

import Auth from '../modules/Login/services/Auth';
import LoginStore from '../modules/Login/stores/LoginStore';
import RedirectStore from '../stores/RedirectStore';
import LoginActions from '../modules/Login/actions/LoginActions';

import LoginFormComponent from '../modules/Login/components/LoginForm/LoginForm';

import TicketListComponent from '../modules/Ticket/components/TicketList/TicketList';
import TicketShowComponent from '../modules/Ticket/components/TicketShow/TicketShow';

import EventListComponent from '../modules/Event/components/EventList/EventList';
import EventFormComponent from '../modules/Event/components/EventForm/EventForm';
import EventShowComponent from '../modules/Event/components/EventShow/EventShow';

import MenuComponent from '../modules/Menu/components/Menu';
import DashboardComponent from './Dashboard/Dashboard';
import LoaderComponent from './Loader/Loader';

import AppConstants from '../constants/AppConstants';

import AppStyle from './AppStyle';

import History from '../services/History';
import MenuActions from "../modules/Menu/actions/MenuActions";
import ResendTicketsComponent from "./Dashboard/ResendTickets";


class App extends React.Component {
  componentDidMount(){

    let _app = this;

    History.history.listen((location) => {
      if(location.pathname === AppConstants.routes.__LOGOUT_PATH){
        LoginActions.logoutUser();
      }

      MenuActions.closeMenu();
    });

    RedirectStore.addChangeListener((e) => {
        if(e.loading){
          let loading = true;
          if(e.finished){
            loading = false;
          }

          _app.setState({isLoading: loading})
        }
    });

    LoginStore.addChangeListener((e) => {
      if(e.logged_in){
        _app.setState({
          loggedIn: true
        });
      }
      if(e.logged_out){
        _app.setState({
          loggedIn: false
        });
      }
    });

    Auth.redirectIfNotLoggedIn(AppConstants.routes.__LOGIN_PATH);
  }

  constructor(props){
    super(props);
    this.lastLoginName = '';
    this.state = {
      loggedIn: Auth.isLoggedIn(),
      loading: false
    };
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className={'wrapper' + (this.state.loading ? ' loading' : '')}>
        { (this.state.loggedIn === true) ? <MenuComponent {...rest} /> : '' }
        <div className={ classes.mainPanel + ' content-wrap-all'}>
          <div className={classes.content}>
            <div className={classes.container}>
              <Switch>
                  <Route exact path='/admin/index.html' component={DashboardComponent} />
                  <Route exact path='/admin/' component={DashboardComponent} />

                  <Route exact path='/admin/resend' component={ResendTicketsComponent} />

                  <Route exact path='/admin/event/tickets/:id' component={TicketListComponent} />

                  <Route exact path='/admin/login' component={LoginFormComponent} />

                  <Route exact path='/admin/ticket' component={TicketListComponent} />
                  <Route exact path='/admin/event' component={EventListComponent} />

                  <Route exact path='/admin/event/edit/:id' component={EventFormComponent} />
                  <Route exact path='/admin/event/create' component={EventFormComponent} />
                  <Route exact path='/admin/event/:id' component={EventShowComponent} />

                  <Route exact path='/admin/ticket/:id' component={TicketShowComponent} />
              </Switch>
            </div>
          </div>
        </div>
        <LoaderComponent {...rest}/>
      </div>
    );
  }
}

App.defaultProps = {};

export default withRouter(withStyles(AppStyle)(App));
