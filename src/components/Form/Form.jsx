import React from 'react';
import IntlTools from '../../services/IntlTools';
import Dropzone from 'react-dropzone'

import DatePicker from "react-datepicker";

import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';
import Button from 'muicss/lib/react/button';
import classNames from 'classnames'

const initialState = {
  data: null,
  loaded: 0,
  files: [],
  form: {
    name: '',
    date: new Date()
  }
};

class FormComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = initialState;
    this.formData = {};
  }

  static extractType(field){
    return field.type;
  }

  static extractValue(field){
    return !!field.value ? field.value : '';
  }

  getFieldName(name){
    return this.formData.name+'[' + name + ']'
  }

  upload(acceptedFiles){
    let files = this.state.files;

    const reader = new FileReader();

    for(let key in acceptedFiles){
      let file = acceptedFiles[key];
      reader.onload = (event) => {
        files = [event.srcElement.result];
        this.setState({files: files});
      };
      reader.readAsDataURL(file);
    }
  }

  buildField(field, name){
    let {classes} = this.props;
    const fieldType = FormComponent.extractType(field);
    //let fieldValue = FormComponent.extractValue(field);

    let id = this.getFieldName(name).replace(']','').replace('[','_');

    let placeholder = '';

    if(!!this.formData.fields[field] && !!this.formData.fields[field].placeholder){
      placeholder = this.formData.fields[field].placeholder;
    }

    if(fieldType === 'textarea'){
      return <div key={name} className='form-group-field'>
        {field.label
          ? <label id={id + '_label'} htmlFor={id}>
            {IntlTools.trans(field.label)}
          </label>
          : ''}
        <Textarea
          placeholder={placeholder}
          data-field-name={name}
          onChange={this.handleChange.bind(this)} name={this.getFieldName(name)}
          id={id} className={this.formData.fieldClasses.concat(field.classes).join(' ')} value={this.state.form[name] ? this.state.form[name] : ''}>
        </Textarea>
      </div>
    }

    if(fieldType === 'file') {
      return (
        <div key={name} className='form-group-field'>
          {field.label
            ? <label id={id + '_label'} htmlFor={id}>
              {IntlTools.trans(field.label)}
            </label>
            : ''}
          <Dropzone multiple={false} onDrop={this.upload.bind(this)}>
          {({getRootProps, getInputProps, isDragActive}) => {
            return (
              <div
                {...getRootProps()}
                className={classNames('dropzone', {'dropzone--isActive': isDragActive})}
              >
                {this.state.files.length > 0 &&
                <div>
                  {this.state.files.map((file, key) => (
                    <img
                      alt="Preview"
                      key={key}
                      src={file}
                      style={{maxHeight: 100, maxWidth: '100%'}}
                    />
                  ))}
                </div>
                }
                <input {...getInputProps()} />
                {
                  isDragActive ?
                    <p>Przeciągnij i upuśc plik tutaj...</p> :
                    <p>Przeciągnij i upusć plik, lub kliknij i wybierz.</p>
                }
              </div>
            )
          }}
        </Dropzone>
      </div>
      );
    }

    return <div key={name} className='form-group-field'>
      {field.label
        ? <label id={id + '_label'} htmlFor={id}>
          {IntlTools.trans(field.label)}
        </label>
        : ''}
      <Input
        type={fieldType}
        placeholder={placeholder}
        data-field-name={name}
        onChange={this.handleChange.bind(this)}
        name={this.getFieldName(name)}
        id={id}
        className={this.formData.fieldClasses.concat(field.classes).join(' ')}
        value={this.state.form[name] ? this.state.form[name] : ''} />
    </div>
  }

  componentWillMount(){

  }

  handleSubmit(e){
    let form = {};

    for(let field in this.state.form){
      let dataValue = this.state.form[field];
      let transform = this.formData.fields[field].transform ? this.formData.fields[field].transform : false;

      if(!!transform){
        dataValue = transform(dataValue);
      }

      form = form.merge({ field: dataValue });
    }

    this.setState({'form': form });
  }

  handleChange(e){
    let val = this.state.form;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.fieldName;
    val[fieldName] = dataValue;

    this.setState({'form': val });
  }

  buildForm(values){
    let _self = this;

    return <Form onSubmit={_self.handleSubmit.bind(_self)} data-route={_self.formData.url} className={_self.formData.classes.join(' ')}>
      {Object.keys(_self.formData.fields).map((key, i) => {
        return _self.buildField(_self.formData.fields[key], key, values);
      })}
      <Button color='primary'>{IntlTools.trans(this.formData.submitLabel)}</Button>
    </Form>

  }
}

export default FormComponent;
