import React from 'react';

import PropTypes from 'prop-types';

import { withStyles } from 'material-ui';

import ResendTicketStyle from './ResendTicketsStyle';

import { injectIntl} from 'react-intl';

import IntlTools from '../../services/IntlTools';
import Input from "muicss/lib/react/input";
import Button from "muicss/lib/react/button";
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import EventActions from "../../modules/Event/actions/EventActions";
import EventApi from "../../modules/Event/services/EventApi";
import EventStore from "../../modules/Event/stores/EventStore";
import EventConstants from "../../modules/Event/constants/EventConstants";
import TicketApi from "../../modules/Ticket/services/TicketApi";
import TicketStore from "../../modules/Ticket/stores/TicketStore";
import DashboardStore from "../../stores/DashboardStore";
import AppConstants from "../../constants/AppConstants";
import DashboardActions from "../../actions/DashboardActions";
import RedirectActions from "../../actions/RedirectActions";

const initialState = {
  emails: [],
  inputs: 1,
  events: [],
  selectedEvent: null,
  checkResults: null,
  loading: false,
  normalize: false,
};

const loader = require('../../../../assets/svg/loading.svg');

class ResendTicketComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentDidMount(){
    let _self = this;

    EventStore.addChangeListener((e) => {
      if(e.list_updated){
        _self.setState({events: EventStore.getItems()});
      }
    });

    TicketStore.addChangeListener((e) => {
      if(!!e.emailsChecked){
        _self.setState({checkResults: e.data.data});
      }

      if(!!e.emailsSent){
        let resetState = initialState;
        delete resetState.events;
        _self.setState(resetState);
      }
    });

    DashboardStore.addChangeListener((e) => {
      if(e.actionType === AppConstants.LOADING){
        _self.setState({loading: e.loading});
      }
    });

    EventApi.getIndex();
  }

  __f(txt){
    return IntlTools.trans(txt);
  }

  handleChange(e){
    let val = this.state.emails;
    let dataValue = e.target.value;
    let fieldName = e.target.dataset.n;
    val[fieldName] = dataValue;

    this.setState({emails: val });
  }

  printInput(n){
    return <Input
      type={'text'}
      onChange={this.handleChange.bind(this)}
      name={'emails[]'}
      data-n={n}
      key={n}
      value={!!this.state.emails[n] ? this.state.emails[n] : ''} />;
  }

  printInputs(){
    let inputs = [];

    for(let n = 0; n < this.state.inputs; n++){
      inputs.push(this.printInput(n));
    }

    return <div id={'inputs_id'}>{inputs.map((elem, i) => {
      return elem;
    })}</div>;
  }

  onChangeEvent(ev) {
    this.setState({selectedEvent: ev.target.value});
  }

  printEvents(){
    let events = [];

    for(let key in this.state.events){
      events.push(<Option key={key} value={this.state.events[key].id} label={this.state.events[key].name}/>);
    }

    return <Select onChange={this.onChangeEvent.bind(this)}  id={'event_id'}>
      <Option value={null} label={this.__f('app.choose')}/>
      {events.map((elem, i) => {
      return elem;
    })}</Select>;
  }

  handleInputs(e){
    this.setState({inputs: e.target.value });
  }

  checkEmails(e){
    TicketApi.getByEmails(this.state.emails, this.state.selectedEvent);
  }

  onCheck(e){
    this.setState({normalize: e.target.checked });
  }

  printForm(){
    return <div>
      <h1>{this.__f('app.resend')}</h1>
      <label>Wydarzenie</label>
      {this.printEvents()}
      <label>Ilość adresów e-mail</label>
      <Input
        type={'text'}
        onChange={this.handleInputs.bind(this)}
        name={'inputs'}
        value={this.state.inputs} />
      <label>Adresy e-mail</label>
      {this.printInputs()}
      {(this.state.emails.length && this.state.selectedEvent) ? <Button  color={'primary'} onClick={this.checkEmails.bind(this)}>{this.__f('app.check_emails')}</Button> : ''}
    </div>;
  }

  printCheckResults(){

    let items = [];

    for(let key in this.state.checkResults){
      items.push(<h2 key={key}>{this.state.checkResults[key].email} - {this.state.checkResults[key].count}</h2>);
    }

    return <div>
      {items.map((elem, i) => {
        return elem;
      })}
      <label style={{display: 'block', textAlign: 'center', fontWeight: 'bold'}}>Normalizuj adres mailowy (wszystkie litery z małej)</label>
      <Input type={'checkbox'} onChange={this.onCheck.bind(this)} />
      <Button color={'primary'} onClick={this.sendTickets.bind(this)}>{this.__f('app.resend')}</Button>
    </div>;
  }

  sendTickets(){
    TicketApi.sendTickets(this.state.emails, this.state.selectedEvent, this.state.normalize);
  }

  render() {
    return (
      <div className="container">
        {this.state.loading ? <div style={{textAlign: 'center'}}><img src={loader} /><br/><strong>{this.__f('app.loading')}...</strong></div> : ''}
        {this.state.checkResults === null ? this.printForm() : this.printCheckResults()}
      </div>
    );
  }
}

ResendTicketComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(ResendTicketStyle)(injectIntl(ResendTicketComponent));
