import React from 'react';
import Table from '../Table/Table';

import PropTypes from 'prop-types';

import { withStyles, Card, CardHeader, Avatar } from 'material-ui';
import dashboardStyle from '../../../vendor/material-dashboard-react/variables/styles/dashboardStyle';

import { injectIntl} from 'react-intl';

import IntlTools from '../../services/IntlTools';
import EventStore from "../../modules/Event/stores/EventStore";
import EventConstants from "../../modules/Event/constants/EventConstants";
import EventApi from "../../modules/Event/services/EventApi";
import TicketStore from "../../modules/Ticket/stores/TicketStore";
import TicketConstants from "../../modules/Ticket/constants/TicketConstants";
import Modal from "react-modal";
import DashboardActions from "../../actions/DashboardActions";
import AppConstants from "../../constants/AppConstants";
import Button from "muicss/lib/react/button";
import QrReader from 'react-qr-scanner'
import TicketApi from "../../modules/Ticket/services/TicketApi";
import Link from "../Link/Link";
import {Redirect} from "react-router-dom";
import LoginStore from '../../modules/Login/stores/LoginStore';

const isMd5 = require('is-md5');

const initialState = {
  logged_in: false,
  percentage: 0,
  scanModal: false
};

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    // width                 : '80%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

const previewStyle = {
  height: 420,
  width: 520,
  maxWidth: '100%'
};

Modal.setAppElement(document.getElementById('admin-app')?'#admin-app':'#app');

class DashboardComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentDidMount(){
    let _self = this;
    EventStore.addChangeListener((e) => {
      if(e.actionType === EventConstants.DASHBOARD_DATA){
        this.setState({ logged_in: !!LoginStore.jwt });
        _self.setState({percentage: e.data.percentage});
      }
    });

    TicketStore.addChangeListener((e) => {
      if(e.actionType === AppConstants.SCANNER_ENABLE || e.actionType === AppConstants.SCANNER_DISABLE){
        _self.setState({scanModal: e.scannerEnabled});
      }

      if(e.actionType === TicketConstants.TICKET_CHECK){
        EventApi.dashboard();

        console.log(e);

        if(e.ticketChecked){
          alert(this.__f('app.ticket.checked') + '. ID: ' + e.ticket.id + ', DATA ZAKUPU: ' + e.ticket.soldDate);
        }else{
          if(e.ticketExists){
            alert(this.__f('app.ticket.not_checked') + '. ID: ' + e.ticket.id + ', DATA KASOWANIA: ' + e.ticket.checkDate);
          }else{
            alert(this.__f('app.ticket.not_exists'));
          }
        }
        this.closeModal();
      }
    });

    EventApi.dashboard();
  }

  handleScan(data){
    if(!!data) {
      TicketApi.checkTicket(data);
      this.closeModal();
    }
  }

  handleError(err){
    console.error(err)
  }

  openModal(){
    DashboardActions.enableScanner();
  }

  afterOpenModal(){
  }

  closeModal(){
    DashboardActions.disableScanner();
  }

  __f(txt){
    return IntlTools.trans(txt);
  }

  render() {
    let title = IntlTools.trans('app.sold_percentage');

    return this.state.logged_in ? (
      <div className="container">
        <Card>
          <CardHeader
            avatar={
              <Avatar aria-label="Front">
                {Math.floor(this.state.percentage)}
              </Avatar>
            }
            title={ title }
          />
        </Card>

        <Button color={'primary'} onClick={this.openModal.bind(this)}>
          {this.__f('app.ticket.check')}
        </Button>

        <Link to={AppConstants.routes.__RESEND_PATH} text={'app.resend'} />

        <Modal
          isOpen={this.state.scanModal}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel={this.__f('app.event.payment')}
          style={customStyles}
        >
          <QrReader
            style={previewStyle}
            onError={this.handleError}
            onScan={this.handleScan.bind(this)}
            facing={'environment'}
          />
        </Modal>
      </div>
    ) : <div></div>;
  }
}

DashboardComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(injectIntl(DashboardComponent));
