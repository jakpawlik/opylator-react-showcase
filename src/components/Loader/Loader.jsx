import React from 'react';
import { Cached } from 'material-ui-icons';

class LoaderComponent extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <div id="loader" className={(this.props.loading) ? 'active' : ''}><Cached /></div>
    );
  }
}

LoaderComponent.defaultProps = {
  loading: false
}

export default LoaderComponent;
