import AppDispatcher from '../dispatchers/AppDispatcher';
import AppConstants from '../constants/AppConstants';

export default {
  enableScanner: () => {
    AppDispatcher.dispatch({
      actionType: AppConstants.SCANNER_ENABLE,
      scannerEnabled: true,
    });
  },
  disableScanner: () => {
    AppDispatcher.dispatch({
      actionType: AppConstants.SCANNER_DISABLE,
      scannerEnabled: false,
    });
  },
  enableLoading: () => {
    AppDispatcher.dispatch({
      actionType: AppConstants.LOADING,
      loading: true,
    });
  },
  disableLoading: () => {
    AppDispatcher.dispatch({
      actionType: AppConstants.LOADING,
      loading: false,
    });
  },
}
