import AppDispatcher from '../dispatchers/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import History from '../services/History';

export default {
  redirect: (route) => {
    AppDispatcher.dispatch({
      actionType: AppConstants.REDIRECT,
      redirect: true,
      route: route
    });
    History.push(route);
  },
  loading: (finished) => {
    AppDispatcher.dispatch({
      actionType: AppConstants.LOADING,
      redirect: false,
      finished: finished
    });
  }
}
