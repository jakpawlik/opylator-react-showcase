import { Ticket } from '../../src/modules/Ticket/models';
import { Event } from '../../src/modules/Event/models';

export default Object.assign({}, [
  Ticket,
  Event
]);
