'use strict';

let path = require('path');
let webpack = require('webpack');
let baseConfig = require('./base');
let defaultSettings = require('./defaults');
const CleanWebpackPlugin = require('clean-webpack-plugin')

// Add needed plugins here
let BowerWebpackPlugin = require('bower-webpack-plugin');

let pathsToClean = [
  'public/assets/*',
]

// the clean options to use
let cleanOptions = {
  root:     path.join(__dirname, '/../../..'),
  verbose:  true,  
  allowExternal: true,
  beforeEmit: true
}

let config = Object.assign({}, baseConfig, {
  entry: [
    './src/index',
    //'../assets/less/app.less',
  ],
  cache: true,
  devtool: 'eval-source-map',
  plugins: [
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new BowerWebpackPlugin({
      searchResolveModulesDirectories: false
    })
  ],
  module: defaultSettings.getDefaultModules()
});

config.module.loaders.push({
  test: /\.(js|jsx)$/,
  loader: 'react-hot!babel-loader',
  include: [].concat(
    config.additionalPaths,
    [
      path.join(__dirname, '/../../assets/svg'),
      path.join(__dirname, '/../src'),
      path.join(__dirname, '/../cfg/models'),
      path.join(__dirname, '/../vendor/material-dashboard-react')
    ]
  )
});

module.exports = config;
